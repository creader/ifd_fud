# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 15:15:21 2015

@author: acrnrms
"""
import numpy as np
import glob
#import cmipdata
########### Find_nearest###################

def find_nearest(array,value):
    """ find_nearest(array,value): returns index of array
        element with nearest value to value
    """
    return np.nanargmin((np.abs(array-value)))



##make_regmask###################
def get_regs(region,ret_str=False):
    """ Get corners or regions

    Parameters:
    -----------
      region : [ string ]
               okh,ber,atl,nwna,cna,aleu,lab,wtpo,cetpo,ctpo               
      
      *ret_str* : Boolean (if true: return string)

    Returns:
    -----------
    reg [lon1,lon2,lat1,lat2]

    """

    if region=='okh':
        reg=[140,170,45,60]    
    elif region=='ber':
        reg=[170,200,55,75]    
    elif region=='atl':
        reg=[350,70,65,80]    
    elif region=='nwna':
        reg=[190,240,50,75]    
    elif region=='cna':
        reg=[240,280,30,55] 
    elif region=='aleu':
        reg=[165,205,40,65]    
    elif region=='lab':
        reg=[300,330,50,60]    
    elif region=='wtpo': 
        reg=[105,150,-5,25]    
    elif region=='cetpo': 
        reg=[150,280,-15,7]
    elif region=='ctpo': 
        reg=[150,200,-10,7]
    elif region=='nino34': 
        reg=[190,240,-5,5]
    else:
        print('ERROR:' + region + ' not defined')

    if ret_str:
        reg=str(reg[0]) + ',' + str(reg[1]) + ',' + str(reg[2]) + ',' + str(reg[3])

    return reg    
    

def make_regmask(lon,lat,region):
    """Create a regional mask for plotting and area-averaging.

    Parameters:
    -----------
      lon : array
      lat : array
      *region* : [ string or list] 
                  list:[lon1,lon2,lat1,lat2]
                      corners of the region
                  string:
                          OKH
                             BER
              ATL
              NWNA
              CNA
              ALEU              
    Returns:
    -----------
      regmask: 2D array (=1 in region, 0 outside) 
      """

    ####determine lon1,lon2,lat1,lat2
    if type(region) == str:
        [lon1,lon2,lat1,lat2]=get_regs(region)        

    elif type(region) == list:    
        [lon1,lon2,lat1,lat2]=region

    ####lonslats

    lons, lats = np.meshgrid(lon,lat) 
    nlon=np.size(lon); nlat=np.size(lat)

    ####regmask
    regmask=np.ones((nlat,nlon))
    regmask[np.logical_or(lats<lat1,lats>lat2)]=0    
    if lon2>lon1:regmask[np.logical_or(lons<lon1,lons>lon2)]=0 
    else:regmask[np.logical_and(lons>lon2,lons<lon1)]=0 
        
    return regmask

##calc_regmeam###################

def calc_regmean(lon,lat,x,region):    
    """Calculate regional mean.

    Parameters:
    -----------
      lon : array
      lat : array
      *region* : [ string or list] 
                  list:[lon1,lon2,lat1,lat2]
                      corners of the region
                  string:
                          OKH
                          BER
                          ATL
                          NWNA
                          CNA
                          ALEU              
    Returns:
    -----------
      regmean 
      """
    #masked coslat
    regmask=make_regmask(lon,lat,region)  

    #masked coslat
    lons, lats = np.meshgrid(lon,lat) 
    coslats=np.cos(lats*np.pi/180)
    coslatsm=np.ma.masked_where(regmask<1,coslats)
    #masked var    
    xm=np.ma.masked_where(regmask<1,x)
    #calc_mean        
    area=np.nansum(coslatsm)    
    sum=np.nansum(coslats*xm)
    if area==0: mean=np.nan
    else: mean=sum/area

    return mean


########### cd_loaddata_0dim###################
# Written 20161007
# cmipdata package normally loads data as follows:
# datadict = cd.loadfiles(ens, 'tas')
# tas_cna=datadict['data']
#
# But this does not work for 0-dim data (e.g. trend of global mean)
# This is a workaround
#
#import cmipdata as cd

def cd_loaddata_0dim(ens,var):
    ifiles   = sorted(ens.lister('ncfile'))
    data=np.zeros((len(ifiles)))
    for i, ifile in enumerate(ifiles):
        data[i] = cd.loadvar(ifile, var).squeeze()
        print(ifile)

    return data

def cd_loaddata_0dim_files(ens,var):
    ifiles   = sorted(ens.lister('ncfile'))
    data=np.zeros((len(ifiles)))
    for i, ifile in enumerate(ifiles):
        data[i] = cd.loadvar(ifile, var).squeeze()
        print(ifile)

    return data




########### cd_realization_mean###################
# 20161019
# cmipdata package ens_stat mean only output full ensemble mean, not by model
#  
import os as os

def cd_realization_means(ens,variable_name, output_prefix=''):
    """ Compute the ensemble mean and standard deviation.

    The ensemble mean and standard deviation is computed over all models-realizations
    and experiments for variable variable_name in ens, such that each model has a weight
    of one. An output file is written containing the ensemble mean and another file is
    written with the standard deviation, containing the names '_ENS-MEAN_' and '_ENS-STD_'
    in the place of the model-name. If the ensemble contains multiple experiments, files
    are written for each experiment.

    The ensemble in ens must be homogenous. That is to say all files must be on the same
    grid and span the same time-frame, within each experiment (see remap, and time_slice for more).
    Additionally, variable_name should have only one filename per realization and experiment. That
    is, join_exp_slice should have been applied.

    The calculation is done by, first computing the mean over all realizations for each model;
    then for the ensemble, calculating the mean over all models.
    The standard deviation is calculated across models using the realization mean for each model.
     
    

        Parameters
    ----------
    ens : cmipdata Ensemble
          The ensemble on which to do the concatenation.

    variable_name : str
                    The name of the variable to be concatenated.


    Returns
    -------
    A tuple of lists containing the names of the mean and standard deviation files created
    The ENS-MEAN and ENS-STD files are written to present working directory.

    Examples
    ---------

    1. Compute the statistics for the ts variable::

        >>cd.ens_stats(ens, 'ts')



    experiment_list = ens.lister('experiment')
    for exname in experiment_list:
        files_to_mean = []
        for model in ens.objects('model'):
            experiment = model.getChild(exname)
            if experiment != None:
                modfilesall = []
                for realization in experiment.children:
                    realization
                    modfilesall.append(realization.getChild(variable_name).children)
    """
    meanfiles = []
    stdevfiles = []
    experiments = {}
    for f in ens.objects('ncfile'):
        table = f.getDictionary()
        if table['variable'] == variable_name:
            if table['experiment'] in experiments:
                experiments[table['experiment']].append([f, table['model']])
            else:
                experiments[table['experiment']] = [[f, table['model']]]
    # multiple output files for multiple experiments
    for experimentname in experiments:
        files_to_mean = []
        models = {}
        for fm in experiments[experimentname]:
            if fm[1] in models:
                models[fm[1]].append(fm[0])
            else:
                models[fm[1]] = [fm[0]]
        for model in models:
            files = models[model]

            fnames = []
            for f in files:
                fnames.append(f.name)

            inputfiles = ''
            for f in fnames:
                inputfiles = inputfiles + ' ' + f
            outfile = output_prefix + os.path.split(fnames[0])[1].replace(files[0].parent.parent.name, 'R-MEAN')
            cdostr = 'cdo ensmean ' + inputfiles + ' ' + outfile

            if os.path.isfile(outfile):
                files_to_mean.append(outfile)
            else:
                os.system(cdostr)
                files_to_mean.append(outfile)


    return meanfiles, stdevfiles

########### cd_mkensemble###################
# 20190416
# add option to mkensemble for cmip6 data files
#  


class DataNode(object):
    """ Defines a cmipdata DataNode.

    Attributes
    ----------
    genre      : string
                 The attribute of DataNode
    name       : string
                 The name of the particular genre
    children   : list
                 List of DataNodees of genre beneath the current DataNode
    parent     : DataNode
                 for genre 'ensemble' the parent is None
    start_date : string
                 for genre 'file'
    end_date   : string
                 for genre 'file'
    realm      : string
                 for genre 'variable' contains the realm of the varaible

    """

    def __init__(self, genre, name, parent=None, **kwargs):
        """ Possible keys in kwargs:
                'start_date'
                'end_date'
                'realm'
        """
        self.genre = genre
        self.name = name
        self.children = []
        self.parent = parent
        for k,v in list(kwargs.items()):
            setattr(self, k, v)

    def add(self, child):
        """ Add DataNode to children

        Parameters
        ----------
        child : DataNode
        """
        self.children.append(child)

    def delete(self, child):
        """Delete DataNode from children

        Parameters
        ----------
        child : DataNode
        """
        self.children.remove(child)

    def getNameWithoutDates(self):
        """ Return string name with the dates removed if present

        Returns
        -------
        string
        """
        return self.name.replace('_' + self.start_date + '-' + self.end_date + '.nc', "")

    def getChild(self, input_name):
        """ Returns DataNode given the name of the DataNode
            if it is in children

        Parameters
        ----------
        input_name : string

        Returns
        -------
        DataNode : Returns None if the DataNode is not in children
        """
        for child in self.children:
            if child.name == input_name:
                return child
        return None

    def mer(self):
        """Returns a generator containing lists of length 3
           with the DataNode genre:'realization'
                the DataNode genre:'experiment'
                string model-experiment-realization

        Returns
        -------
        generator
        """
        for obj in self.objects('realization'):
            yield [obj, obj.parent,
                   obj.parent.parent.name + '-' +
                   obj.parent.name + '-' +
                   obj.name]

    def lister(self, genre, unique=True):
        """ Returns a list of names of a particular genre

        Parameters
        ----------
        genre : string
                the genre of returned list
        unique: boolean
                if True removes duplicates from the list
        Return
        ------
        list of strings
        """
        def alist(item, genre):
            if item.genre == genre:
                yield item.name
            else:
                for child in item.children:
                    for value in alist(child, genre):
                        yield value
        if unique:
            return list(set(alist(self, genre)))
        else:
            return list(alist(self, genre))

    def objects(self, genre):
        """ Returns a generator for a DataNode of a particular genre

        Parameters
        ----------
        genre : string
                the genre of returned generator

        Return
        ------
        generator of DataNodees
        """
        def alist(item, genre):
            if item.genre == genre:
                yield item
            else:
                for child in item.children:
                    for value in alist(child, genre):
                        yield value
        return list(alist(self, genre))

    def parentobject(self, genre):
        """ Returns the parent DataNode of a particular genre

        Parameters
        ----------
        genre : string
                the genre of returned DataNode

        Return
        ------
        DataNode
        """
        def check(item):
            if item.genre == genre:
                return item
            else:
                return check(item.parent)
        return check(self)


    def _checkfile(self):
        """ Removes files from ensemble if they are not in the directory
        """
        for f in self.objects('ncfile'):
            if not os.path.isfile(f.name):
                f.parent.delete(f)
        
        
    def squeeze(self):
        """ Remove any empty elements from the ensemble
        """
        self._checkfile()
        def sq(node):
            if node.children == [] and node.genre != 'ncfile':
                delete = node
                if node.genre != 'ensemble':
                    node = node.parent
                    node.delete(delete)
                    print('Removing ' + delete.name + ' from ' + delete.parent.name)
                    sq(node)
            for n in node.children:
                sq(n)
        for n in self.children:
            sq(n)

    def getDictionary(self):
        """Returns a dictionary which
           has the genres and their names for all the ancestors of
           the DataNode
        """
        node = self
        values = {}
        while node.genre != 'ensemble':
            values[node.genre] = node.name
            node = node.parent
        return values

    def sinfo(self, listOfGenres=['variable', 'model', 'experiment', 'realization', 'ncfile']):
        """ Returns the number of models, experiments, realizations, variables and files
        in the DataNode"""
        print("This ensemble contains:")
        for key in listOfGenres:
            if key == 'realization':
                print(str(len(list(self.objects(key)))) + " " + key + "s")
            else:
                print(str(len(self.lister(key))) + " " + key + "s")    
                  
    def fulldetails(self):
        """  prints information about the number of models,
             experiments, variables and files ina DataNode tree.
        """
        for model in self.children:
            print(model.name + ':')
            for experiment in model.children:
                print('\t' + experiment.name)
                for realization in experiment.children:
                    print('\t\t' + realization.name)
                    for variable in realization.children:
                        print('\t\t\t' + variable.name)
                        for filename in variable.children:
                            print('\t\t\t\t' + filename.name)

    def fulldetails_tofile(self, fi):
        """  prints information about the number of models,
             experiments, variables and files ina DataNode tree.
        """
        with open(fi, 'w') as f:
            for model in self.children:
                f.write(model.name + ':\n')
                for experiment in model.children:
                    f.write('\t' + experiment.name + '\n')
                    for realization in experiment.children:
                        f.write('\t\t' + realization.name + '\n')
                        for variable in realization.children:
                            f.write('\t\t\t' + variable.name + '\n')
                            for filename in variable.children:
                                f.write('\t\t\t\t' + filename.name + '\n')

def cd_mkensemble(filepattern, filenames=None, experiment='*', prefix='', kwargs='',mip=''):
    """Creates and returns a cmipdata ensemble from a list of
    filenames matching filepattern.

    Optionally specifying prefix will remove prefix from each filename
    before the parsing is done. This is useful, for example, to remove
    pre-pended paths used in filepattern (see example 2).

    Once the list of matching filenames is derived, the model, experiment,
    realization, variable, start_date and end_date fields are extracted by
    parsing the filnames against a specified file naming convention. By
    default this is the CMIP5 convention, which is::

        variable_realm_model_experiment_realization_startdate-enddate.nc

    If the default CMIP5 naming convention is not used by your files,
    an arbitary naming convention for the parsing may be specified by
    the dicionary kwargs (see example 3).


    Parameters
    ----------

    filepattern : string
                A string that by default is matched against all files in the
                current directory. But filepattern could include a full path
                to reference files not in the current directory, and can also
                include wildcards.

    filenames : list
                A list of all files to be used to make the ensemble. Can be used 
                as an altenative to specifying a filepattern.
                
    prefix : string
             A pattern occuring in filepattern before the start of the official
             filename, as defined by the file naming converntion. For instance,

    mip : string
             String that describes mip to which the file format corresponds (cmip5 or cmip6)

    EXAMPLES
    --------

    1. Create ensemble of all sea-level pressure files from the historical experiment in
    the current directory::

        ens = mkensemble('psl*historical*.nc')

    2. Create ensemble of all sea-level pressure files from all experiments in a non-local
    directory::

        ens = mkensemble('/home/ncs/ra40/cmip5/sam/c5_slp/psl*'
                      , prefix='/home/ncs/ra40/cmip5/sam/c5_slp/')

    3. Create ensemble defining a custom file naming convention::

        kwargs = {'separator':'_', 'variable':0, 'realm':1, 'model':2, 'experiment':3,
                  'realization':4, 'dates':5}

        ens = mkensemble('psl*.nc', **kwargs)

    """
    if filenames is None:
        # find all files matching filepattern
        filenames = sorted(glob.glob(filepattern))

    if kwargs == '':
        if mip=='cmip5':
            kwargs = {'separator': '_', 'variable': 0, 'realm': 1, 'model': 2, 'experiment': 3,
                  'realization': 4, 'dates': 5}
        elif mip=='cmip6':
            kwargs = {'separator': '_', 'variable': 0, 'realm': 1, 'model': 2, 'experiment': 3,
                      'realization': 4, 'dates': 6}
        else: #default       
          kwargs = {'separator': '_', 'variable': 0, 'realm': 1, 'model': 2, 'experiment': 3,
                  'realization': 4, 'dates': 5}

    # Initialize the ensemble object
    ens = DataNode('ensemble', 'ensemble')

    # Loop over all files and
    for name in filenames:
        name = name.replace(prefix, '')
        path, name = os.path.split(name)
        variablename = name.split(kwargs['separator'])[kwargs['variable']]
        realm = name.split(kwargs['separator'])[kwargs['realm']]
        modelname = name.split(kwargs['separator'])[kwargs['model']]
        experiment = name.split(kwargs['separator'])[kwargs['experiment']]
        realization = name.split(kwargs['separator'])[kwargs['realization']]
        dates = name.split(kwargs['separator'])[kwargs['dates']]
        start_date = name.split(kwargs['separator'])[kwargs['dates']].split('-')[0]
        end_date = name.split(kwargs['separator'])[kwargs['dates']].split('-')[1].split('.')[0]

        # create the model if necessary
        m = ens.getChild(modelname)
        if m is None:
            m = DataNode('model', modelname, parent=ens)
            ens.add(m)

        # create the experiment if necessary
        e = m.getChild(experiment)
        if e is None:
            e = DataNode('experiment', experiment, parent=m)
            m.add(e)

        # create the realization if necessary
        r = e.getChild(realization)
        if r is None:
            r = DataNode('realization', realization, parent=e)
            e.add(r)

        # create the variable if necessary
        v = r.getChild(variablename)
        if v is None:
            v = DataNode('variable', variablename, parent=r, realm=realm)
            r.add(v)

        filename = (prefix + name)
        # create the file if necessary
        f = v.getChild(filename)
        if f is None:
            f = DataNode('ncfile', filename, parent=v, start_date=start_date, end_date=end_date)
            v.add(f)

    ens.sinfo()
    print('\n For more details use ens.fulldetails() \n')
    return ens


########### calc_normfit###################
from scipy.stats import norm
def calc_normfit(input,verb=True):
# From Kelly, 20160225

    """ use scipy.stats.norm.fit() to calc a pdf mean and sigma

            input: 1d array of data

                   default # of return vals is 500

            returns: fittedpdf, mean, sigma, x values
    """

    # calc the pdf associated with the hist
    inpdf=norm.fit(input)
    mn=inpdf[0]
    sgm=inpdf[1]

    #Generate X points
    xlims = [-4*sgm+mn, 4*sgm+mn] # large limits
    xx = np.linspace(xlims[0],xlims[1],500)
    #Get Y points via Normal PDF with fitted parameters
    pdf_fitted = norm.pdf(xx,loc=mn,scale=sgm)

    if verb:
        print('===== mean ' + str(mn) + ' sigma ' + str(sgm))

    return pdf_fitted, mn, sgm, xx

########### calc_kernel###################

def calc_kernel(input):
# From Kelly, 20160225

    """ use scipy.stats.gaussian_kde() to calc a pdf 

            input: 1d array of data
            returns pdf, x values
    """
    
    kernel = sp.stats.gaussian_kde(input)
    pdf,mn,sgm,xx= calc_normfit(input) # just to get x values
    
    # will calculate the kernel for each xx; 
    # the more xx, the smoother the curve
    return kernel(xx), xx



