#!/bin/sh
set -xe
basedir="/HOME/mcr/new_IFD" # root of directory tree, set in master.py 
nrtdir=""  # nsidc subdirectory containing near real-time data, set in master.py
# directory structure of nsidc data
freq="daily"
force="off"
reg="north"
hem="nh"
# set up raw data directory if necessary
  mkdir -p $basedir/DATA/
  mkdir -p $basedir/DATA/IN_DAILY_SIC_OBS
  mkdir -p $basedir/DATA/IN_DAILY_SIC_OBS/RAW
cd $basedir/DATA/IN_DAILY_SIC_OBS/RAW
echo reg=$reg hem=$hem
echo "wget data"
wget -N -nd -r -A "*.nc" ftp://sidads.colorado.edu/pub/DATASETS/NOAA/${nrtdir}/$reg/${freq}/ 

