#!/bin/sh
set -e
basedir=/HOME/mcr/new_IFD # root of directory tree, set in master.py 
ncdir="G02202_V4" # nsidc subdirectory containing latest processed data, set in master.py
# directory structure of nsidc data
freq="daily" 
reg="north"
hem="nh"
# set up raw data directory if necessary
  mkdir -p $basedir/DATAy
  mkdir -p $basedir/DATA/IN_DAILY_SIC_OBS
  mkdir -p $basedir/DATA/IN_DAILY_SIC_OBS/RAW
  cd $basedir/DATA/IN_DAILY_SIC_OBS/RAW
# wget data
  echo "wget data"
  # download data
  wget -N -nd -r -A "*.nc" ftp://sidads.colorado.edu/pub/DATASETS/NOAA/${ncdir}/$reg/${freq}/


