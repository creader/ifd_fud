#!/usr/bin/env python
#######################################################
# Note: TTD indicates things that still need to be done.
#######################################################
# RECIPE TO UPDATE daily sic file
#
# 1) update inputfiles using <Slava's script>: #TTD replace script name, add other instructions
#    Make sure basedir is set correctly. 
#
# 2) run this script
#    Make sure user, basedir, rundir and codedir are properly specified.
#    Check other parameters in "Basic User-specified parameters" section
#################################################33
# PROGRAM: 1a_interp_sic_obs.py
# 
# PURPOSE: Interpolate Sea-ice data linearly to fill in missing data
#          and convert to grid conventions used by a forecast model. 
# 
# AUTHOR: Cathy Reader March 27, 2018, added comments by Michael Sigmond September 2018
#         Updated version, Cathy Reader, July 2020, added user switches to define directories,
#         unpack files to make less version-dependent. Oct 2021, changes made to facilitate
#         operationalization.
#
#         Operational version, November, 2021 by Cathy Reader 
#
# INPUT FILES: NSIDC Sea ice concentration data files in netcdf format,  placed in:
#              <basedir>DATA/IN_DAILY_SIC_OBS/RAW/
#              by running <Slava's scritp> TTD replace scrpt name.
#
#              A sample forecast or other file containing the desired grid information, in:
#              <basedir>DATA/IN_AUX_FILES/
#              This must contain all the attributes set up for cdo sellonlatbox and the desired
#              remapping operator to work. This can be done with ncatted if necessary.
#              
#              An optional File with land mask in same format as the output grid. (This file
#              can also be used as the sample file, above.)
#                   
# STEPS: 1) From daily NSIDC files, extract the SIC variable ('make single variable files'),
#           and the region north of 40N.  (There is an option, save_files, for them to be saved
#           in the SAVE directory for later use in next steps, mainly used to save time in
#           debugging.)       
#        2) Interpolate in time to fill in dates and places with no valid data
#        3) Interpolate spatially to the output (forecast) grid
#        4) Optional masking with supplied mask on output grid.
#
# OUTPUT FILE: <basedir>DATA/IN_ 
#
# ##############################################################################
# IMPORT NECESSARY MODULES:
# #########################
import sys
import os.path # module for dealing with directories
import subprocess as sbp # module for running system commands
import matplotlib.pyplot as plt # basic plotting module
import time # used for putting timestamp on created files
import numpy as np # python scientific computing package
from scipy import interpolate # interpolation modeule
import numpy.ma as ma #masked array module
# netCDF4 package to manipulate netCDF fiiles.
from netCDF4 import Dataset  # https://code.google.com/p/netcdf4-python/      
# #############################################################################
# BASIC USER-SET PARAMETERS:
# ###########################
user_string='jellyfish' # Information about user to include in netcdf files.
# Set up code-containing, running and data root directories for each user. 
basedir='albatross'
rundir='monkey'
codedir='sloth'
sys.path.append(codedir + 'MODS') #Include modules for plotting and other tools
#
firstyeara=2015 # first year to add to file 
firstmontha=1 # first month to add to file
firstyearfo=1981 # first year of file to append to
firstmonthfo=1 # first month of file to append to
lastyeara=2021 #last year to add to file.   
lastmontha=10 # last month to add to file   
lastyearin=2014 # last year of file to append to
lastmonthin=12 # last month of file to append to
nrt_year=2021# year that near-real-time data starts
nrt_yearf=2021 # year that near-real-time data starts in file to append to
nrt_month=6# year that near-real-time data starts
nrt_monthf=6 # year that near-real-time data starts in file to append to
nc_version=""
nrt_version=""
#
# #############################################################################
# "ADVANCED" USER-SET PARAMETERS:
# ################################
applynewmask = 'off' # Apply forecast land mask to regridded file
latcut= 39.5 # approximate Southern latitude cutoff to reduce grid size
polem='1' # method of dealing with pole hole:
           # 1: fill with 1
           # f: leave flagged
           # g: interpolate using griddata (very slow!)
remaptype = 'remapcon' # cdo remapping operator to use, options include:
##                       remapcon (conservative remapping - requires grid corners)
##                       remapbil (bilinear remapping)
##                       remapdis (distance-weighted remapping)
## Some flags to save time by saving/using previously generated files (mainly for debugging/testing): 
save_files ='off' # if 'on', save single-variable files in "SAVE" directory. 
use_saved_files='off' # use premade single-varible files from "SAVE" directory
                      # set to on if you want to skip step 1 (and all saved files exist)
if use_saved_files=='on': save_files=on # If you use saved files but don't have save_files=on,
                                        # you'll lose them!                                   
save_merged_file='off' # save merged file (mainly for debigging. Usually off since new concattenation will fail
                       # if old version exists)
use_filled_file='off' # use previously-made file with missing data filled in time 
                      # set to on if you only want to do the remapping (step3)
inname='seaice_conc_daily_nh' # prefix of original nsidc files 
inname_nrt='seaice_conc_daily_icdr_nh' # prefix of near-real-time nsidc files
varn='cdr_seaice_conc'  # sea ice concentration variable name in input file                       
varn_nrt='cdr_seaice_conc' # sea ice concentration variable name in nrt files file
timdim_nrt='tdim' # time dimension name in nrt files
timdim='tdim' # time dimension name in nrt files
ydim_nrt='y' # y dimension name in nrt files
xdim_nrt='x' # x dimension name in nrt files
# DIRECTORIES:
indir=basedir+'DATA/IN_AUX_FILES/' # directory containing sample grid file and optional mask file.
indirobs=basedir+'DATA/IN_DAILY_SIC_OBS/RAW/';indirmerged=basedir+'DATA/IN_DAILY_SIC_OBS/MERGED/'
outdir = indirmerged # In general, we will want to output to the input directory for subsequent scripts.
indirobs_nrt=indirobs # this can be set to a different directory if desired
# Grid and mask info
forgridname='1x1'# name to use for output grid
samplename='ifd_1980_2012_clim_im06_1x1_grid' # sample target grid file without .nc extension.
maskname='' # land mask file without .nc extension
landvar='' # variable name containing landmask info in landmask file  
landvalue=0 # value of land in landmask file
#gridfile_nrt=indir + 'seaice_conc_daily_icdr_nh_f18_20190811_v01r00.nc' # file containing nrt grid info
gridfile_nrt=indir + 'nsidcgrid.nc' # file containing nrt grid info
# #####################################################################################################
# PREPARATIONS
# ############
# record hash of current version
ghcmd='git rev-parse HEAD'
hash=sbp.check_output(["git","rev-parse","HEAD"])
hash=str(hash,'utf-8').strip('\n')
# set internal variables to corresponding inputs from master_sic.py
lastmonth=lastmonthin
lastyear=lastyearin
firstyearar=firstyeara
firstmonthar=firstmontha
lastyearar=lastyeara
lastmonthar=lastmontha
firstmonthfos="%02d" % (firstmonthfo)
firstmonthars="%02d" % (firstmonthar)
lastmonthars="%02d" % (lastmonthar)
lastmonths="%02d" % (lastmonth)
nrt_months="%02d" % (nrt_month)
nrt_monthfs="%02d" % (nrt_monthf)
yeartrim=nrt_yearf
montrims="%02d" % (nrt_monthf-1)
if montrims =='00': yeartrim=nrt_yearf-1; montrims='12'        
#
nrt_time=str(nrt_year)+nrt_months
nrt_timef=str(nrt_yearf)+nrt_monthfs
timdimu=timdim 
# Get rid of old nrt_data, if necessary 
if nrt_time != nrt_timef: 
    origfile = outdir + inname +'_'+varn+'_'+str(firstyearfo)+firstmonthfos+'_'+str(lastyear)+lastmonths+'_nrt_'+nrt_timef+'_filled_1_1x1.nc'
    trimmedfile = outdir + inname +'_'+varn+'_'+str(firstyearfo)+firstmonthfos+'_'+str(yeartrim)+montrims+'_nrt_'+nrt_timef+'_filled_1_1x1.nc'
    cmds='cdo seldate,'+str(firstyearfo)+'-01-01T00:00:00'+','+str(yeartrim)+'-'+montrims+'-31T24:60:60 '+ origfile + ' '+ trimmedfile
    sbp.call(cmds,shell=True)
    # reset years and months to append to trimmed file
    lastyear=yeartrim
    lastmonths=montrims
    firstyearar=nrt_yearf
    firstmonthar=nrt_monthf
    firstmonthars=nrt_monthfs
    lastmonthar=nrt_month-1
    lastyearar=nrt_year
    if lastmonthar == 0: lastyearar=nrt_year-1; lastmonthar=12        
# additional directories and filenames:
if not os.path.isdir(outdir):os.makedirs(outdir)
if (save_files=='on')|(use_saved_files=='on'):    
    savedir = basedir +'DATA/SAVE/'  
    if not os.path.isdir(savedir):os.makedirs(savedir)    
mergedfile_append = outdir + inname +'_'+varn+'_'+str(firstyearar)+firstmonthars+'_'+str(lastyearar)+lastmonthars+'_nrt_'+nrt_time+'.nc'
mergedfile_appendp = outdir + inname +'_'+varn+'_'+str(firstyearar)+firstmonthars+'_'+str(lastyearar)+lastmonthars+'_nrt_'+nrt_time+'p.nc'
outfile_previous=indirmerged + inname +'_'+varn+'_'+str(firstyearfo)+firstmonthfos+'_'+str(lastyear)+lastmonths+'_nrt_'+nrt_timef+'_filled_'+polem+'_'+forgridname+'.nc'
filledfile         =outdir + inname +'_'+varn+'_'+str(firstyearar)+firstmonthars+'_'+str(lastyearar)+lastmonthars+'_nrt_'+nrt_time+'_filled_'+polem+'.nc'  
filledfilencg=outdir + inname +'_'+varn+'_'+str(firstyearar)+firstmonthars+'_'+str(lastyearar)+lastmonthars+'_nrt_'+nrt_time+'_filled_'+polem+'_ncg.nc'
outfile       = outdir +inname +'_'+varn+'_'+str(firstyearfo)+firstmonthfos+'_'+str(lastyearar)+lastmonthars+'_nrt_'+nrt_time+ '_filled_'+polem+'_'+forgridname+'.nc' #regridded file
outfilea       = outdir +inname +'_'+varn+'_'+str(firstyearar)+firstmonthars+'_'+str(lastyearar)+lastmonthars+ '_nrt_'+nrt_time+'_filled_'+polem+'_'+forgridname+'.nc' #regridded file
outfile_masked= outdir +inname +'_'+varn+'_'+str(firstyearfo)+firstmonthfos+'_'+str(lastyearar)+lastmonthars+'_nrt_'+nrt_time+ '_filled_'+polem+'_'+forgridname+'_masked.nc' #regridded file with new land mask 
# ###############################################
# STEP 1) EXTRACT SIC VARIABLES FROM DAILY FILES 
# ###############################################
if use_filled_file !='on':
 ## Make single-variable files if necessary
 if use_saved_files !='on': # DOESN"T CURRENTLY WORK FOR NRT FILES 
    print('Step 1) Making single-variable files')      
    ## Loop through years to add, months and days   
    for year in range(firstyearar,lastyearar+1):    
        print(year)
        lastmonthl=12
        firstmonthl=1
        if year==lastyearar:lastmonthl=lastmonthar
        if year==firstyearar:firstmonthl=firstmonthar
        for month in range(firstmonthl,lastmonthl+1):
            months= "%02d"%(month)
            for day in range(1,32):
                found_file=False
                days= "%02d"%(day)
                ## select sea-ice concentration from NSIDC files
                outfilep = outdir + inname+'_'+str(year)+months+days+'_'+nc_version+'_'+varn # single-variable file  
                for instr in ['n07','f08','f11','f13','f17']: # loop through instruments
                    infile= indirobs + inname+'_'+str(year)+months+days+'_'+instr+'_'+nc_version
                    if os.path.isfile(infile+'.nc'):
                       found_file=True
                       ncsv='ncks -C -v '+varn+',xgrid,ygrid,time' +' '+infile+'.nc '+ rundir+'jtmp.nc'
                       sbp.call(ncsv,shell=True)
                       ncmu='ncpdq -U '+rundir+'jtmp.nc ' + outfilep+'.nc' #unpack file
                       sbp.call(ncmu,shell=True)
                       sbp.call('/bin/rm '+rundir+'jtmp.nc',shell=True)                                              
                if year>= nrt_year and month >= nrt_month: # add Near Real Time data
                    timdimu=timdim_nrt
                    outfilep = outdir + inname+'_'+str(year)+months+days+'_'+nrt_version+'_'+varn # single-variable file  
                    infile= indirobs_nrt + inname_nrt+'_'+str(year)+months+days+'_f18_'+nrt_version # format may change over time.
                    if os.path.isfile(infile+'.nc'):   
                       found_file=True
                       cmd='ncks -v '+varn+' ' +infile+'.nc '+ rundir+'jtmp.nc'
                       sbp.call(cmd,shell=True)
                       ncmu='ncpdq -U '+rundir+'jtmp.nc ' + outfilep+'.nc' #unpack file
                       sbp.call(ncmu,shell=True)
                       sbp.call('/bin/rm '+rundir+'jtmp.nc',shell=True)                                              
                if not found_file: 
                  if [month,day] not in [[2,29],[2,30],[2,31],[4,31],[6,31],[9,31],[11,31]]:  
                   print('NB, inputfile for {},{},{} not found'.format(year,month,day))       
 else:
      print('Skipping Step 1) using already-made saved single-variable files') 
      ## Loop through years to add, months and days  
      for year in range(firstyearar,lastyearar+1):    
          print(year) 
          for month in range(firstmonthar,lastmonthar+1):
              months= "%02d"%(month)
              for day in range(1,32): 
                  days= "%02d"%(day)
                  mcd='mv '+savedir+ inname+'_'+str(year)+months+days+'_'+nc_version+'_'+varn+'.nc '+ outdir
                  if os.path.isfile(savedir+ inname+'_'+str(year)+months+days+'_'+nc_version+'_'+varn+'.nc'): 
                     sbp.call(mcd,shell=True)      
################################################### 
# STEP 2a) merge daily files into a timeseries file
###################################################
 print('Step 2a: Merge daily files into a timeseries file')      
 ## Concatenate single-variable files  
 ncm='ncrcat '+ outdir + inname+'*v0*.nc '+mergedfile_append
 sbp.call(ncm,shell=True)
 print('made merged file')
 ## Save or remove single-variable files
 if (save_files) == 'on':
     sbp.call('mv '+ outdir + inname+'*.nc ' + savedir,shell=True )
 else:
     sbp.call( '/bin/rm -f ' +outdir + inname+'*v0*.nc',shell=True)        
##############################################################################
#STEP 2b)  Fill missing data in merged file by linear interpolation in time 
##############################################################################
 print('Step 2b) Fill missing data in merged file by linear interpolation in time')      
 print('input:'+ mergedfile_append + ' output:' + filledfile)
 indc=Dataset(mergedfile_append,'r')
 if str(firstyearar)+firstmonthars != str(firstyearfo)+firstmonthfos:   
   indcp=Dataset(outfile_previous,'r')
 else:
   indcp=Dataset(mergedfile_append,'r')     
 timea=indc.variables['time'] # times with data in mergedfile
 nt=len(timea)
 ## Access first sea-ice record to obtain masks of flagged points
 sicn0=indc.variables[varn][0,:,:] 
 sicn0.mask=ma.nomask
# landmask=((sicn0<-.015)&(sicn0>-.025)).astype(int) # land in original NSIDC file
 landmask=(sicn0>2.53)&(sicn0<2.55).astype(int) # land in nsidc files
 coastmask=((sicn0<2.54)&(sicn0>2.52)).astype(int) # coastal points, treated as land
 unusedmask=((sicn0<2.53)&(sicn0>2.51)).astype(int) # mostly lakes, treated as land
 ## Create masks of points that are always open ocean,and points with missing data 
 oceanmask=np.ones((sicn0.shape))
 yxmiss = np.zeros((sicn0.shape))
 for it in range(nt):
  sicn=indc.variables[varn][it,:,:].filled()
  yxmiss[sicn[:]>5.]=1
  oceanmask[((sicn>.0000001)|(sicn<-.0000001))]=0
 nt_full=int(timea[-1]-timea[0])+1 # length of time record in filled file
 if (nt_full>len(timea)): # if there aren't files for every day, all points need interpolation
    print(' Missing days. All points need interpolation') 
    yxmiss[(landmask[:]!=1)&(oceanmask[:]!=1)&(coastmask[:]!=1)]=1
 indices = np.arange(nt_full) # time indices in filled file 
 interppts=np.where(yxmiss==1) # points that need interpolating
 landpts=np.where(landmask==1) # land points
 oceanpts=np.where(oceanmask==1) # always open ocean points
 coastpts=np.where(coastmask==1)
 unusedpts=np.where(unusedmask==1)
 ## Interpolation   
 fld_full = np.zeros((nt_full,len(interppts[0])))*np.nan # array to contain interpolated data
 if len(interppts[0])>0:
  ## put values from raw data into appropriate spot in array to interpolate
  for it in range(nt):
    sicn=indc.variables[varn][it] 
    fld_full[int(timea[it]-timea[0])] = sicn[interppts]
  ## replace missing data values, and days with no data anywhere with NaNs     
  fld_full[fld_full > 5.] = np.nan #anywhere with no data set to NaN  
  ## Loop over points that need interpolation
  for ipt in range(len(interppts[0])):    
   not_nan =np.logical_not(np.isnan(fld_full[:,ipt]))
   if len(indices[not_nan]) > 1: 
            ## Interpolate point ipt 
            tempinterp = np.interp(indices, indices[not_nan], fld_full[not_nan,ipt])
            ## Replace data for point ipt with interpolated version
            fld_full[:,ipt] = tempinterp   
 print('finished interpolation')
 #
 ## Output filled file (without grid cell corners)
 outnc = Dataset(rundir+'jtmp.nc','w',format='NETCDF3_CLASSIC')
 varnames_previous=list(indcp.variables.keys())# variable names in file to append to
 ## Copy dimensions from merged file
 for dname, the_dim in indc.dimensions.items():
     if dname == timdim_nrt: dname=timdim # switch nrt time dim to processed if necessary
     outnc.createDimension(dname, len(the_dim) if not the_dim.isunlimited() else None)
 ## Create variables,
 for v_name, varin in indc.variables.items():
  print(v_name)
  if v_name!=varn: # Copy variables except sea-ice concentration from merged file
    if v_name in varnames_previous:  
      if v_name != 'latitude' and v_name != 'longitude' and v_name != 'time':
        outVar = outnc.createVariable(v_name, varin.datatype,indcp.variables[v_name].dimensions )
        outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})
        outVar[:] =varin[:]  
      elif v_name=='time': # make sure time dimension is processed nsidc value and add filled values
        outVar = outnc.createVariable(v_name,'float',(timdim))
        outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})
        outVar[:] = np.arange(timea[0], timea[0]+ nt_full)
        print('corrected time variable')
      else: # get any variables missing from nrt data,
        outVar = outnc.createVariable(v_name, varin.datatype,indc.variables[v_name].dimensions )
        outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()}) 
    elif v_name=='xgrid' or v_name=='ygrid':
      outVar = outnc.createVariable(v_name, varin.datatype,varin.dimensions )
      outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})
      outVar[:] =varin[:]        
  else:
   # insert filled data
   outVar = outnc.createVariable(v_name,'float',(timdim,varin.dimensions[1],varin.dimensions[2]))   
   for it in range(nt_full):
    ## put in data from points that didn't require interpolation   
    try:
      vslice=np.squeeze(indc.variables[varn][timea==it+timea[0],:,:])
      vslice.mask=ma.nomask
      if np.min(vslice) != np.max(vslice): # check time slice likely has valid data 
       polepts=np.where((vslice<2.52)&(vslice>2.5)) # pole hole for most recent valid data
      else:
       print('bad file, time=', it+timea[0])   
    except:
      vslice=np.zeros((sicn0.shape))
    ## Put in interpolated data  
    vslice[interppts]=fld_full[it]
    ## Fill pole hole
    if polem=='1': # Option to just fill with 1's 
     vslice[polepts]=1     
    elif polem=='g': # Option to interpolate spatially using griddata
     vslice[polepts]=np.nan
     x = np.arange(0, vslice.shape[1])
     y = np.arange(0, vslice.shape[0])
     vslicem= np.ma.masked_invalid(vslice)
     xx, yy = np.meshgrid(x, y)
     #get only the valid values
     x1 = xx[~vslicem.mask]
     y1 = yy[~vslicem.mask]
     newarr = vslicem[~vslicem.mask]
     if (len(polepts[0]) !=0)&(len(polepts[0]) != vslice.size):         
       vslice = interpolate.griddata((x1, y1), newarr.ravel(),(xx, yy),method='cubic')
       vslice[vslice>1.]=1.
     else:
       print(it, timea[0]+it, 'no pole interpolation ', len(polepts[0])) 
    else : # Option to just flag pole hole        
     vslice[polepts]=-5  
    ## Flag land, open ocean
    vslice[landpts]=-2
    vslice[coastpts]=-2
    vslice[unusedpts]=-2
    vslice[oceanpts]=0
    outVar[it] = vslice
 outnc.creation_date = time.ctime(time.time())
 outnc.created_by = user_string+', git hash: '+hash  
 outnc.close()
 # rename time dimension to keep cdo operators happy
 rnd='ncrename -d '+timdim+',time -d x,xgrid -d y,ygrid ' + rundir+'jtmp.nc '+filledfilencg
 sbp.call(rnd,shell=True)
 sbp.call('/bin/rm '+rundir+'jtmp.nc',shell=True)                                              
 print('made filled file')
 del fld_full
 del sicn
 ## Save or remove merged file (if it stays in the working directory, nrcat will hang next time)
 if (save_merged_file) == 'on':
  sbp.call('mv '+ mergedfile_append+' ' + savedir,shell=True )
 else:
  sbp.call('/bin/rm '+mergedfile_append, shell=True)
  nct='ncatted -a coordinates,'+varn+',c,c,"longitude latitude"' +' ' + filledfilencg
  sbp.call(nct,shell=True)
 if remaptype=='remapcon': # need to connect lats and lons with variable and add grid corners
   ## Add approximate grid corners (replace if actual NSIDC grid corners are obtained)
   ## Most longitudes
   indll=Dataset(gridfile_nrt,'r')  
   ind=Dataset(filledfilencg,'r')
   lons= indll.variables['longitude'][:]
   lats= indll.variables['latitude'][:]
   # make latitude and longitude variables

   loc1=np.zeros((lons.shape))
   loc2=np.zeros((lons.shape))
   loc3=np.zeros((lons.shape))
   loc4=np.zeros((lons.shape))
   for i in range(1,lons.shape[0]-1):
    for j in range(1,lons.shape[1]-1):
      if ((i-j<=82)&(i-j>=78))&(j<=153): # In most cases, average lats and lons of neighbouring centres                
       a=np.array([lons[i,j],lons[i-1,j],lons[i-1,j+1],lons[i,j+1]])
       b=np.zeros((4))
       b[a>0]=1
       nm=np.sum(b)       
       loc1[i,j]=(lons[i,j]+lons[i-1,j]+lons[i-1,j+1]+lons[i,j+1]-nm*360)/4.
       if loc1[i,j]<-180:loc1[i,j]=360+loc1[i,j]       
       a=np.array([lons[i,j],lons[i+1,j],lons[i+1,j+1],lons[i,j+1]])
       b=np.zeros((4))
       b[a>0]=1
       nm=np.sum(b)              
       loc2[i,j]=(lons[i,j]+lons[i+1,j]+lons[i+1,j+1]+lons[i,j+1]-nm*360)/4.
       if loc2[i,j]<-180:loc2[i,j]=360+loc2[i,j]       
       a=np.array([lons[i,j],lons[i+1,j],lons[i+1,j-1],lons[i,j-1]])
       b=np.zeros((4))
       b[a>0]=1
       nm=np.sum(b)
       loc3[i,j]=(lons[i,j]+lons[i+1,j]+lons[i+1,j-1]+lons[i,j-1]-nm*360)/4. 
       if loc3[i,j]<-180:loc3[i,j]=360+loc3[i,j]                   
       a=np.array([lons[i,j],lons[i-1,j],lons[i-1,j-1],lons[i,j-1]])
       b=np.zeros((4))
       b[a>0]=1
       nm=np.sum(b)
       loc4[i,j]=(lons[i,j]+lons[i-1,j]+lons[i-1,j-1]+lons[i,j-1]-nm*360)/4.
       if loc4[i,j]<-180:loc4[i,j]=360+loc4[i,j]       
      else: # Near lon=180 move all points to same sign convention before averaging    
       loc1[i,j]=(lons[i,j]+lons[i-1,j]+lons[i-1,j+1]+lons[i,j+1])/4.
       if loc1[i,j]<-180:loc1[i,j]=360+loc1[i,j]
       loc2[i,j]=(lons[i,j]+lons[i+1,j]+lons[i+1,j+1]+lons[i,j+1])/4.
       if loc2[i,j]<-180:loc2[i,j]=360+loc2[i,j]
       loc3[i,j]=(lons[i,j]+lons[i+1,j]+lons[i+1,j-1]+lons[i,j-1])/4.
       if loc3[i,j]<-180:loc3[i,j]=360+loc3[i,j]
       loc4[i,j]=(lons[i,j]+lons[i-1,j]+lons[i-1,j-1]+lons[i,j-1])/4.
       if loc4[i,j]<-180:loc4[i,j]=360+loc4[i,j]         
   ## Most latitudes
   lac1=np.zeros((lats.shape))
   lac2=np.zeros((lats.shape))
   lac3=np.zeros((lats.shape))
   lac4=np.zeros((lats.shape))
   for i in range(1,lons.shape[0]-1):
    for j in range(1,lons.shape[1]-1):
       lac1[i,j]=(lats[i,j]+lats[i-1,j]+lats[i-1,j+1]+lats[i,j+1])/4.
       lac2[i,j]=(lats[i,j]+lats[i+1,j]+lats[i+1,j+1]+lats[i,j+1])/4.
       lac3[i,j]=(lats[i,j]+lats[i+1,j]+lats[i+1,j-1]+lats[i,j-1])/4.      
       lac4[i,j]=(lats[i,j]+lats[i-1,j]+lats[i-1,j-1]+lats[i,j-1])/4.
   ## Near pole 
   lac1[234,153]=90
   lac2[233,153]=90
   loc4[233,154]=135
   lac3[233,154]=90
   lac4[234,154]=90
   ## Edges of grid
   ym=lons.shape[1]-1
   xm=lons.shape[0]-1
   for j in range(1,lons.shape[1]-1):
    lac2[0,j]=lac1[1,j] 
    lac3[0,j]=lac4[1,j]
    lac1[0,j]=2*lats[0,j]-lac3[0,j]
    lac4[0,j]=2*lats[0,j]-lac2[0,j]
    loc2[0,j]=loc1[1,j] 
    loc3[0,j]=loc4[1,j]
    loc1[0,j]=2*lons[0,j]-loc3[0,j]
    loc4[0,j]=2*lons[0,j]-loc2[0,j]
    xm=lons.shape[0]-1
    lac1[xm,j]=lac2[xm-1,j]
    lac3[xm,j]=lac4[xm-1,j]
    lac2[xm,j]=2*lats[xm,j]-lac3[xm,j]
    lac4[xm,j]=2*lats[xm,j]-lac2[xm,j]
    loc1[xm,j]=loc2[xm-1,j]
    loc3[xm,j]=loc4[xm-1,j]
    loc2[xm,j]=2*lons[xm,j]-loc3[xm,j]
    loc4[xm,j]=2*lons[xm,j]-loc2[xm,j]
   for i in range(1,lons.shape[0]-1):
    lac1[i,0]=lac4[i,1] 
    lac2[i,0]=lac3[i,1]
    lac3[i,0]=2*lats[i,0]-lac1[i,0]
    lac4[i,0]=2*lats[i,0]-lac2[i,0]
    loc1[i,0]=loc4[i,1] 
    loc2[i,0]=loc3[i,1]
    loc3[i,0]=2*lons[i,0]-loc1[i,0]
    loc4[i,0]=2*lons[i,0]-loc2[i,0]
    if i==80: loc3[i,0]=loc3[i,0]+360      
    lac4[i,ym]=lac1[i,ym-1]
    lac3[i,ym]=lac2[i,ym-1]
    lac1[i,ym]=2*lats[i,ym]-lac3[i,ym]
    lac2[i,ym]=2*lats[i,ym]-lac4[i,ym]
    loc4[i,ym]=loc1[i,ym-1]
    loc3[i,ym]=loc2[i,ym-1]
    loc1[i,ym]=2*lons[i,ym]-loc3[i,ym]
    loc2[i,ym]=2*lons[i,ym]-loc4[i,ym]
   ## Corners 
   lac1[0,0]=lac4[0,1]
   lac2[0,0]=lac4[1,1]
   lac3[0,0]=lac4[1,0]
   lac4[0,0]=2*lats[0,0]-lac2[0,0] 
   loc1[0,0]=loc4[0,1]
   loc2[0,0]=loc4[1,1]
   loc3[0,0]=loc4[1,0]
   loc4[0,0]=2*lons[0,0]-loc2[0,0]
   lac2[0,ym]=lac1[1,ym]
   lac3[0,ym]=lac1[1,ym-1]
   lac4[0,ym]=lac1[0,ym-1]
   lac1[0,ym]=2*lats[0,ym]-lac3[0,ym] 
   loc2[0,ym]=loc1[1,ym]
   loc3[0,ym]=loc1[1,ym-1]
   loc4[0,ym]=loc1[0,ym-1]
   loc1[0,ym]=2*lons[0,ym]-loc3[0,ym]
   lac1[xm,ym]=lac2[xm-1,ym]
   lac3[xm,ym]=lac2[xm,ym-1]
   lac4[xm,ym]=lac2[xm-1,ym-1]
   lac2[xm,ym]=2*lats[xm,ym]-lac4[xm,ym]
   loc1[xm,ym]=loc2[xm-1,ym]
   loc3[xm,ym]=loc2[xm,ym-1]
   loc4[xm,ym]=loc2[xm-1,ym-1]
   loc2[xm,ym]=2*lons[xm,ym]-loc4[xm,ym]
   lac1[xm,0]=lac3[xm-1,1]
   lac2[xm,0]=lac3[xm,1]
   lac4[xm,0]=lac3[xm-1,0]
   lac3[xm,0]=2*lats[xm,0]-lac1[xm,0]
   loc1[xm,0]=loc3[xm-1,1]
   loc2[xm,0]=loc3[xm,1]
   loc4[xm,0]=loc3[xm-1,0]
   loc3[xm,0]=2*lons[xm,0]-loc1[xm,0]
   #
   ##Output file with grid corners
   outnc = Dataset(filledfile,'w',format='NETCDF3_CLASSIC')
#   ind=Dataset(filledfilencg,'r')
   for dname, the_dim in ind.dimensions.items(): # copy dimensions	
          outnc.createDimension(dname, len(the_dim) if not the_dim.isunlimited() else None)
   for v_name, varin in indll.variables.items(): # copy latitude and longitude
       if v_name=='latitude' or v_name=='longitude':
           outVar = outnc.createVariable(v_name, varin.datatype, varin.dimensions)
           outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})
           outVar[:] =varin[:]
       ## Connect grid corners to center lats and lons
       if v_name == 'latitude': 
             outVar.bounds = 'grid_corner_lat'
       if v_name == 'longitude':
             outVar.bounds = 'grid_corner_lon'             
    
   for v_name, varin in ind.variables.items(): # copy variables
           outVar = outnc.createVariable(v_name, varin.datatype, varin.dimensions)
           outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})
           if varin.name == varn:
             for iv in range(0,varin.shape[0]):               
              outVar[iv,:] =varin[iv,:]
           else:
             if v_name!='projection':
              outVar[:] =varin[:] 
   ## create grid corner dimensions, variables and fill             
   outnc.createDimension('grid_corners',4)
   outVar=outnc.createVariable('grid_corner_lat','float',(ind.variables[varn].dimensions[1],ind.variables[varn].dimensions[2],'grid_corners')) 
   outVar[:,:,0]=lac4 
   outVar[:,:,1]=lac3 
   outVar[:,:,2]=lac2 
   outVar[:,:,3]=lac1
   outVar=outnc.createVariable('grid_corner_lon','float',(ind.variables[varn].dimensions[1],ind.variables[varn].dimensions[2],'grid_corners'))
   outVar[:,:,0]=loc4 
   outVar[:,:,1]=loc3 
   outVar[:,:,2]=loc2 
   outVar[:,:,3]=loc1  
   outnc.creation_date = time.ctime(time.time())
   outnc.created_by = user_string+', git hash: '+hash 
   outnc.close()
   rmncg= '/bin/rm ' + filledfilencg
   sbp.call(rmncg,shell=True) # remove file without grid corners
 else: # corner grid already made 
   rnff = 'mv ' +filledfilencg + ' ' + filledfile    
   sbp.call(rnff,shell=True)
# set grid_info
   sgc='cdo setgrid,' +gridfile_nrt +' '+filledfile+' '+rundir+'jtmp.nc'
   sbp.call(sgc,shell=True)
   sbp.call('mv ' +rundir+'jtmp.nc ' + filledfile,shell=True)
# ###############################
# STEP 3:  Remap to forecast grid 
# ###############################
print('Step 3: Remap to Forecast grid and write to ' + outfilea)      
sample_file= indir+samplename+'.nc'# file containing target grid info
## do the remapping
cmd='cdo '+remaptype+','+sample_file +' -setmissval,-2 -del29feb '+ filledfile + '  ' +outfilea
sbp.call(cmd,shell=True)
if str(firstyearfo)+firstmonthfos != str(firstyearar)+firstmonthars:
 # check that there is no hole between new file and file to append to
 lasta=Dataset(outfile_previous,'r').variables['time'][-1]
 firsta=Dataset(outfilea,'r').variables['time'][0]
 if firsta-lasta != 1.:
    print('Interpolating in time between files') 
    cmds='cdo seltimestep,1' +' '+ outfilea + ' '+rundir+'jtmpa'
    sbp.call(cmds,shell=True)
    cmds='cdo seltimestep,'+str(len(Dataset(outfile_previous,'r').variables['time'])) + ' '+ outfile_previous + ' '+rundir+'jtmpp'
    sbp.call(cmds,shell=True)
    cmdmt='cdo mergetime '+rundir+'jtmpp ' +rundir+'jtmpa ' +rundir+'jtmp'
    sbp.call(cmdmt,shell=True)
    cmdi='cdo intntime,'+str(int(firsta-lasta)) +' '+rundir+'jtmp '+rundir+'jtmpfp'
    sbp.call(cmdi,shell=True)
    cmds='cdo seltimestep,2 '+rundir+'jtmpfp '+rundir+'jtmpf'
    sbp.call(cmds,shell=True)
    cmdmt='cdo mergetime ' + outfilea +' '+rundir+'jtmpf '+ outfile_previous +' '+outfile
    sbp.call(cmdmt,shell=True)
    cmdrm = '/bin/rm '+rundir+'jtmp '+rundir+'jtmpp '+rundir+'jtmpa '+rundir+'jtmpfp '+rundir+'jtmpf'
    sbp.call(cmdrm,shell=True)
 else:
    cmdmt='cdo mergetime ' + outfilea +' '+ outfile_previous +' '+outfile
    sbp.call(cmdmt,shell=True)
 # Remove filled file
cmdrm = '/bin/rm '+ filledfile 
sbp.call(cmdrm,shell=True) # comment out to keep the filled file
print('removed '+filledfile)
# ####################################
# STEP 4:  Apply a landmask (optional) 
# ####################################
## fill land and put forecast land mask on top if desired
if applynewmask=='on':
   print('Step 4: Apply a landmask, and write to ' + outfile_masked)      
   ## cut unused latitudes from mask file
   mask_filec=outdir + maskname +'_N_.nc' # reduced lat version of mask_file
   mask_file= indir+maskname+'.nc'# file containing land mask for target grid 
   cmdcm='cdo sellonlatbox,0,360,'+str(latcut)+',90' +' '+ mask_file + '  ' + mask_filec
   sbp.call(cmdcm,shell=True)
   # read regridded file
   outd = Dataset(outfile,'r') 
   ## Get forecast grid land points 
   landng=Dataset(mask_filec,'r')
   landmaskng=np.squeeze(landng.variables[landvar][:])
   landptsng=np.where(landmaskng==landvalue)
   outncf = Dataset(outfile_masked,'w',format='NETCDF3_CLASSIC') # final output file
   times=outd.variables['time'][:] # time data from regridded file
   ## Copy dimensions and variables from regridded file
   for dname, the_dim in outd.dimensions.items():	
    outncf.createDimension(dname, len(the_dim) if not the_dim.isunlimited() else None)
   for v_name, varin in outd.variables.items():
    #if v_name!=varn: # variable other than sea ice to be copied over 
    outVar = outncf.createVariable(v_name, varin.datatype, varin.dimensions)
    if v_name!=varn:
     outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})  
    if v_name == varn:
     for it in range(len(times)):  
       vslice=np.squeeze(outd.variables[varn][it,:,:]) # original regridded values
       vslice[vslice==-2]=np.nan # set land points to nan
       x = np.arange(0, vslice.shape[1]) # x coordinates
       y = np.arange(0, vslice.shape[0]) # y coordinates
       vslicem= np.ma.masked_invalid(vslice) # mask out nans (land points)
       xx, yy = np.meshgrid(x, y) # x,y grid
       ## Get the valid values
       x1 = xx[~vslicem.mask]
       y1 = yy[~vslicem.mask]
       newarr = vslicem[~vslicem.mask]  
       ## Interpolate over land     
       vslice = interpolate.griddata((x1, y1), newarr.ravel(),(xx, yy),method='cubic')
       ## make sure values are between 0 and 1
       vslice[vslice>1.]=1.
       vslice[vslice<0.]=0.
       ## Apply forecast grid land mask and set points that couldn't be interpolated to land
       vslice[landptsng]=-2
       vslice[np.isnan(vslice)]=-2
       outVar[it] = vslice # fill sea ice variable  
    else:
     outVar[:] =varin[:] # copy data for other variables
   outncf.creation_date = time.ctime(time.time())
   outncf.created_by = user_string+', git hash: '+hash  
   outncf.close() 






