#!/usr/bin/env python
# ------------------------------------------------------------------------------------
# 
# PROGRAM: 2_calc_ifd+fud.py
# 
# AUTHORS: Cathy Reader and Michael Sigmond: Operational python3 version, November, 2021
# 
# PURPOSE: Calculate Ice Free Date and Freeze Up Dates for Observations and Forecasts
# 
# ------------HISTORY---------------
#
# October/November 2021: C Reaeder- Improve directory structure, loop thorough models and obs, covert to py3 and update doc.  
#
# May 2021: M Sigmond- Removed reordering for GEM_NEMO files since timesteps are correct in updated files
# 
# July 2020: M Sigmond- Added user switch, output filename containing threshold and nsd. Cleaned up a bit.
#            
# March 2018: C Reader - modified for general grid and more general file formats.
# 
# April 25/2016: M Sigmond (cleaned up)
# 
# NOV 16/2015: C READER (Further clean-up, restructuring of functions to work on pre-interpolated sea-ice data, minor bug fixes)
# 
# OCT 9/2015: C READER (further clean-up and efficiency improvements, consolodated output files, restricted to Northern Sea-ice area
#                       loops over initialization months, extended to allow CanSIPS model input, inproved plotting
#                       removal of SIC climatology calculation, fud/ifd iteration using a single function, put interpolation bug fix, 
#                       slight change to make model and observed ifd/fud calculations treat the initialization day the same, added
#                       "noise-reduction" by requiring points to remain frozen/melted for a set number of days before declaring fud/ifd)    
# 
# AUG 13/2015: M. SIGMOND (clean up program to calculated IFD and FUD, old name: melt_fud_nasa_final_hbavg.py )
# 
# 
# INPUT FILES: Output from running 1a_interp_sic_obs.py on nsidc data 
#              for relevant forecast grid:
#              <basedir>DATA/IN_DAILY_SIC_OBS/MERGED/seaice_conc_daily_nh_cdr_seaice_conc_<firstyearf><firstmonthf>_<lastyearf><lastmonthf>_nrtyear_<nrt_year>_<insuffix> 
#              and corresponding netcdf sea-ice seasonal forecast files:
#              <indirpreff><init month>/<model>/<inprefix><model>_<insuffix>_i<year><imon_str>01_1.nc  
#
# OUTPUT FILES: <outdir>tn<threshold>_<ifd/fud>_<firstyearf><firstmonthf>_<lastyearf><lastmonthf>_timeseries_im<init month>_<insuffix>.nc: yearly ifd/fud
#               <outdir>tn<threshold>_<ifd/fud>_<firstyearf><firstmonthf>_<lastyearf><lastmonthf>_clim_im<init month>_<insuffix>.nc: climatology 
# ##############################################################################
# Typical usage:
# 1. Set imon (initialization month), and event definition parameters threshold for SIC (threshold), and sequential day requiment (nsd) 
# 2. run obs over hindcast period (e.g. 1981-2019) and, if available for verification, the forecast year (e.g. 2020_2020)
# 3. run model1 (CanCM4) hindcast period (e.g. 1980-2019), and forecast (e.g. 2020_2020)
# 4. repeat #3 for model2 (GEM-NEMO)
# ##############################################################################
# IMPORT EXTERNAL MODULES:
import os.path # module for dealing with directories
import time  # used for putting timestamp on created files
import numpy as np # import numpy to work with arrays/matrices
import subprocess as sbp
import matplotlib.pyplot as plt 
import sys
from netCDF4 import Dataset       # https://code.google.com/p/netcdf4-python/      
# ##############################################################################
# BASIC USER-SET PARAMETERS:
# ##########################
# Directories:
user_string='Cathy Reader, CCCma' # Information about user to include in netcdf files
basedir=''
rundir=''
codedir=''
indirpreff=''
mon_init_list=np.array([10]) # list of months to process 
firstyearfo=1981
lastyearfo=2021 #first and last year in observed daily sic file - not used for forecast 
firstmonthfo=1
lastmonthfo=5 #first and last month in observed daily sic file 
firstyearo=1981
lastyearo=2020 #first and last year to calculate ifd/fud for obs
firstmontho=1
lastmontho=12 #first and last month to calculate ifd/fud for obs
firstyearm=1981
lastyearm=2021 #first and last year to calculate ifd/fud for models
firstmonthm=1
lastmonthm=12 #first and last month to calculate ifd/fud for models
nrt_year=2021
nrt_month=1
if not os.path.isdir(rundir):os.makedirs(rundir) 
sys.path.append(codedir + 'MODS')
import nc as nc # Contains function to easily get netCDF variabbles 
import rms_plots as rpl
# ###############################
# "ADVANCED" USER-SET PARAMETERS:
# ###############################
# nsd: number of days needed to stay frozen/melted to confirm fud/ifd, threshold: sea-ice concentration indicating freeze/melt has occured
# # Operational values for nsd and threshold are 10 and .5
nsd = 10;
threshold = .5 
testplots='off' # Set to 'on' to make rough plots of ifd, fud and fraction never melted/frozen.
indict={'NASAmerged':'obs','CanCM4':'gcm','GEM_NEMO':'gcm'} # dictionary of type for each model/obs name
suffdict={'NASAmerged':'filled_1_1x1','CanCM4':'NEW_1x1_grid','GEM_NEMO':'1x1_grid'} # dictionary of input file suffixes
latcut=39.5 # process only points North of this latitude
nensf=10 # number of forecast ensemble members, not used for obs
ext_mask='on' # mask using externally applied file?
# Definition of melt and freeze seasons
# day before start of month
jd_start = np.array([0,31,59,90,120,151,181,212,243,273,304,334])  
# day before start freeze season (1 Oct ) or 0 for current freeze season
fud_min = np.array([0,0,0,273,273,273,273,273,273,273,273,273]) 
# end freeze season (1 Apr), or last day of forecast range
fud_max = np.array([91,91,91,455,455,455,455,455,455,455,455,455]) 
# day before start meltseason (1 Apr)
#ifd_min = np.array([90,90,90,90,90,90,455,455,455,455,455,455]) # settings for quantifying skill; from im07 onwards: look at next year (GRL paper convention)
ifd_min = np.array([90,90,90,90,90,90,90,90,455,455,455,455]) # setting for plotting operational forecasts: from im09 onward: look at next year
#end melt season (1 Oct)
#ifd_max = np.array([273,273,273,273,273,273,546,577,608,638,638,638]) # settings for quantifying skill; from im07 onwards: look at next year
ifd_max = np.array([273,273,273,273,273,273,273,273,608,638,638,638]) # setting for plotting operational forecasts: from im09 onward: look at next year
##Directories
maskfile=basedir+'DATA/'+'IN_AUX_FILES/GEM_NEMO_oceanmask_final.nc' # file for final masking of data
# ############
# PREPARATION:
# ############
# record hash of current version
ghcmd='git rev-parse HEAD'
hash=sbp.check_output(["git","rev-parse","HEAD"])
hash=str(hash,'utf-8').strip('\n')
oceanmask = nc.getvar(maskfile,'sic')[...]
imon_init_list=mon_init_list-1 
firstmonthfos="%02d" % (firstmonthfo)
lastmonthfos="%02d" % (lastmonthfo)
nrt_months="%02d" % (nrt_month)
nrt_time=str(nrt_year)+nrt_months                                                                                                                               
# There are also some model-dependent parameters in the "Model-dependent prep" section. 
# ################################################################################
# Function to calculate ifd or fud from a single sea-ice concentration timeseries 
def calc_ifdfud(event,nsd,sic,threshold,jd_start,fm_min,fm_max):            
        """
        ifdfud(event,nens,nsd,sic,threshold,initmonth,fm_min,fm_max,ifdfud,nifdfud) 
        Inputs: event: 'fud' to calculate freeze-up, 'ifd' to calculate ice-free day
                nsd:  number of days to stay frozen/melted to qualify  
                sic: timeseries of sea-ice concentrations
                threshold: sea-ice concentration threshold distinguishing frozen and ice-free states
                jd_start: Julian day before start of initialization month
                fm_min/fm_max: min/max values fo the freeze/melt days
        Returns : ifdfud: fud/ifd "Julian Day" for current/upcoming freeze/melt season based on 
                  the day sic cross threshold and stays there for at least nsd subsequent days  
        """
        #######################################################################
        # Computes fud/ifd for a timseseries of sea-ice concentrations         
        #######################################################################
        # initialize
        already=0; never=0
        # land mask
        lmask = np.isnan(sic).astype(int)
        #timeseries of 0's and 1's, depending on whether frozen/melted condition is satisfied
        if event == "ifd":
            mask_positives_tmp = (sic <= threshold).astype(int)      
        else:
            mask_positives_tmp = (sic > threshold).astype(int)
        #set fud/ifd initially to minimum value 
        ifdfud = max(jd_start,fm_min)      
        # sub-timeseries that includes all days in current/upcoming freeze/melt season +
        # nsd-1 extras to check noise-reduction condition for final days
        mask_positives = mask_positives_tmp[max(jd_start,fm_min) - jd_start:fm_max - jd_start + nsd -1]
        # indices where condition is satisfied
        indices_positives=np.where(mask_positives==1)[0]
        if lmask[0]==0:  
            if len(indices_positives) >= nsd :
                iday = 0
                ifdfud = max(jd_start,fm_min)
                done = False
                # check if first day satisfies condition
                if (indices_positives[0]==0)&(indices_positives[nsd-1] == indices_positives[0] + nsd - 1): 
                    done = True
                    already=1.
                # loop through days condition is satisfied and test that it remains so for nsd-days
                # until valid fud/ifd found or subset timeseries is exhausted 
                while not (done | (iday+nsd > len(indices_positives))):
                    if indices_positives[iday+nsd-1] == indices_positives[iday] + nsd - 1:  
                        done = True
                        ifdfud=indices_positives[iday]+max(jd_start,fm_min) + 1     
                    iday=iday+1
                # set default values when valid fud/ifd not found 
                if (not done) & (mask_positives[0] == 1): 
                    ifdfud = max(jd_start,fm_min)
                    already=1.    
                if (not done) & (mask_positives[0] == 0): 
                    ifdfud = fm_max
                    never=1.
            # if there are less than nsd days that the condition is satisfied and it is
            # set default max value   
            else:
                ifdfud = fm_max
                never=1.
        #set value to  0 over land 
        else:ifdfud=0
        #
        return ifdfud,already,never
for model in ['NASAmerged', 'CanCM4', 'GEM_NEMO']:
# #####################
# Model-dependent prep:
 input_type=indict[model]   
 insuffix=suffdict[model]
 ########### OBS file info
 if input_type=='obs': # 'obs' or 'gcm'
  nyears=lastyearo-firstyearo+1
  firstmonths="%02d" % (firstmontho)
  lastmonths="%02d" % (lastmontho)
  firstyear=firstyearo; lastyear=lastyearo; firstmonth=firstmontho; lastmonth=lastmontho
  varn='cdr_seaice_conc' # sea ice concentration variable name
  infile='{}DATA/IN_DAILY_SIC_OBS/MERGED/seaice_conc_daily_nh_cdr_seaice_conc_{}{}_{}{}_nrt_{}_{}'.format(basedir,str(firstyearfo),firstmonthfos,str(lastyearfo),lastmonthfos,nrt_time,insuffix) # Input file
  usefile='{}seaice_conc_daily_nh_cdr_seaice_conc_{}{}_{}{}_nrt_{}_{}'.format(rundir,str(firstyearfo),firstmonthfos,str(lastyearfo),lastmonthfos,nrt_time,insuffix) # Input file
  dimt=0 # index of time dimension in input file
  dimx=1 # index of first spatial dimension in input file
  dimy=2 # index of second spatial dimension in input file
  timvar='time_counter' #name of time variable in original file (currently time for obs, time_counter for forecasts)
  nens=1
########### Forecast Model file info
 if input_type=='gcm': # 'obs' or 'gcm'
  nyears=lastyearm-firstyearm+1       
  inprefix='sic_daily_CCCma-'
  firstmonths="%02d" % (firstmonthm)
  lastmonths="%02d" % (lastmonthm)  
  firstyear=firstyearm; lastyear=lastyearm; firstmonth=firstmonthm; lastmonth=lastmonthm
  timvar='time' #name of time variable in original file (currently time for obs, time_counter for forecasts)
  varn='sic' # sea ice concentration variable name
  dimt=0 # index of time dimension in input file
  dimx=1 # index of first spatial dimension in input file
  dimy=2 # index of second spatial dimension in input file        
  nens=nensf
 ## Set up output files
 outdir  = '{}DATA/IFD_RAW/{}/'.format(basedir,model) 
 plotdir = '{}PLOTS/{}/{}/'.format(basedir,model,str(threshold)+'_'+str(nsd))
 if not os.path.isdir(outdir):os.makedirs(outdir) 
 if not os.path.isdir(plotdir):os.makedirs(plotdir) 
#    
 for imon in imon_init_list:
    print('month: ',imon+1) 
    imon_str = "%02d" % (imon+1) # 2-character string version of month
    if input_type == 'gcm': # Need to set for location of forecast files
        indir='{}{}/{}/SIC/daily/'.format(indirpreff,imon_str,model)
        infilep='{}{}{}_{}'.format(indir,inprefix,model,insuffix)
        usefilep='{}{}{}_{}'.format(rundir,inprefix,model,insuffix)
        infile='{}{}{}_{}_i{}{}01_1'.format(indir,inprefix,model,insuffix,str(firstyear),imon_str)# for grid info
        usefile='{}{}{}_{}_i{}{}01_1'.format(rundir,inprefix,model,insuffix,str(firstyear),imon_str)
## Make single-variable version of first input file and cut off unused latitudes
    ncsv='ncks -v '+varn +' '+infile+'.nc '+ usefile+'_'+varn+'.nc'
    sbp.call(ncsv,shell=True)
    cmdcm='cdo sellonlatbox,.5,360,'+str(latcut)+',90' +' '+ usefile+'_'+varn +'.nc'+ '  ' + usefile+'_'+varn +'c.nc'
    sbp.call(cmdcm,shell=True)        
# Read first sea-ice concentration value
    ind=Dataset(usefile+'_'+varn+'c.nc','r')        
    sicno0=ind.variables[varn][0:1]
    try:
       lat=ind.variables['latitude']
       lon=ind.variables['longitude']
    except:
       lat=ind.variables['lat']
       lon=ind.variables['lon']            
# remove temporary single variable and 'cut' file
    rmc='/bin/rm '+usefile+'_'+varn+'.nc '+ usefile+'_'+varn+'c.nc'
    sbp.call(rmc,shell=True)
    ifd=np.zeros((nyears,nens,sicno0.shape[dimx],sicno0.shape[dimy]));fud=np.zeros((nyears,nens,sicno0.shape[dimx],sicno0.shape[dimy]))
    af=np.zeros((nyears,nens,sicno0.shape[dimx],sicno0.shape[dimy])); nf=np.zeros((nyears,nens,sicno0.shape[dimx],sicno0.shape[dimy]))
    am=np.zeros((nyears,nens,sicno0.shape[dimx],sicno0.shape[dimy])); nm=np.zeros((nyears,nens,sicno0.shape[dimx],sicno0.shape[dimy]))            
    ## Loop over years
    for year in range(firstyear,lastyear+1):
        print(year)
        if input_type == 'gcm':# Need to read new file each year for forecast to fill sic timeseries
             if (dimx==2): # All ensemble members in one file  
                infile = '{}_i{}{}01.nc'.format(infilep,str(year),imon_str)
                usefile= '{}_i{}{}01.nc'.format(usefilep,str(year),imon_str)
                ## Make single-variable version of first input file cut off unused latitudes and make sure 
                # it is sorted chornologically
                ncsv='ncks -v '+varn +' '+infile + infile+'_'+varn+'.nc'
                sbp.call(ncsv,shell=True)
                cmdcm='cdo sellonlatbox,.5,360,'+str(latcut)+',90' +' '+ infile+'_'+varn+'.nc' + '  ' +usefile+'_'+varn+'c.nc'               
                sbp.call(cmdcm,shell=True)  
                sic = nc.getvar(infile+'_'+varn+'c.nc',varn)[:,:,:,:] # get sea-ice concentration
             else: # Separate files for ensemble members
                for ie in range(1,nens+1):
                  infile = '{}_i{}{}01_{}'.format(infilep,str(year),imon_str,str(ie))
                  usefile = '{}_i{}{}01_{}'.format(usefilep,str(year),imon_str,str(ie))
                  ## Make single-variable version of first input file, cut off unused latitudes 
                  # and make sure it's sorted choronologically
                  ncsv='ncks -v '+varn +' '+infile+'.nc '+ usefile+'_'+varn+'.nc'
                  sbp.call(ncsv,shell=True)
                  cmdcm='cdo sellonlatbox,.5,360,'+str(latcut)+',90' +' '+ usefile+'_'+varn+'.nc' + '  ' +usefile+'_'+varn+'c.nc'               
                  sbp.call(cmdcm,shell=True)         
                  if (ie == 1):
                   sicp = nc.getvar(usefile+'_'+varn+'c.nc',varn)[:,:,:]
                   sic=np.expand_dims(sicp,axis=1)
                  else:    
                   sicp = nc.getvar(usefile+'_'+varn+'c.nc',varn)[:,:,:]
                   sicpe=np.expand_dims(sicp,axis=1)
                   sic = np.append(sic,sicpe,axis=1) # get sea-ice concentration 
                  ## Remove temporary files
                  rmc='/bin/rm '+ usefile+'_'+varn+'c.nc '+ usefile+'_'+varn+'.nc '
                  sbp.call(rmc,shell=True)
        else: # Just need to select correct year from single file for obs. to fill sic timeseries
            sic = np.expand_dims(ind.variables[varn][(year-firstyearfo)*365 +jd_start[imon]:(year-firstyearfo+1)*365 + jd_start[imon]],axis=1)
        ## Use function to calculate ifds/fuds
        iyear=year-firstyear
        for ilat in range(sicno0.shape[dimx]):
            for ilon in range(sicno0.shape[dimy]):
                for ens in range(nens):
                    fud[iyear,ens,ilat,ilon], af[iyear,ens,ilat,ilon], nf[iyear,ens,ilat,ilon] = calc_ifdfud('fud',nsd,sic[:,ens,ilat,ilon],threshold,jd_start[imon],fud_min[imon],fud_max[imon])
                    ifd[iyear,ens,ilat,ilon], am[iyear,ens,ilat,ilon], nm[iyear,ens,ilat,ilon] = calc_ifdfud('ifd',nsd,sic[:,ens,ilat,ilon],threshold,jd_start[imon],ifd_min[imon],ifd_max[imon])
    ifd = ifd * oceanmask
    fud = fud * oceanmask
    ## End loop over years
    ## Calculate FUD and IFD climatologies
    ifdclim = np.mean(np.mean(ifd,axis=0),axis=0)
    fudclim = np.mean(np.mean(fud,axis=0),axis=0)
    aff = np.mean(np.mean(af,axis=0),axis=0) # Fraction always frozen
    nff = np.mean(np.mean(nf,axis=0),axis=0) # Fraction never frozen
    amf = np.mean(np.mean(am,axis=0),axis=0) # Fraction always melted
    nmf = np.mean(np.mean(nm,axis=0),axis=0) # Fraction never melted    
    ## Fraction of years where ifd/fud is well-defined (as opposed to having the default value assigned to never_frozen/melted points)
    ifd_def = np.ones( (sicno0.shape[dimx],sicno0.shape[dimy])) - nmf - amf
    fud_def = np.ones( (sicno0.shape[dimx],sicno0.shape[dimy])) - nff - aff 
    ###########################################################################
    #######WRITE NetCDF TIMESERIES FILES#######################################
    ###########################################################################
    ## Make netcdf files of annual fud and ifd    
    tfields = {'fud' : fud , 'ifd' : ifd }   
    for var in ['fud', 'ifd']:
        ## set up output file
        outfile = '{}tn{}_{}_{}_{}{}_{}{}_timeseries_im{}_{}.nc'.format(outdir,str(threshold),str(nsd),str(var),str(firstyear),firstmonths,str(lastyear),lastmonths,imon_str,insuffix)
        print('output file:',outfile)
        outnc = Dataset(outfile,'w',format='NETCDF3_CLASSIC')
        for dname, the_dim in ind.dimensions.items(): # copy dimensions from input file	 
          outnc.createDimension(dname, len(the_dim) if not the_dim.isunlimited() else None)
        try:
          outnc.createDimension('time',lastyear-firstyear+1)
        except:
          print('time dimension already exists')  
        if (dimx == 1):
            outnc.createDimension('ensemble',nens)
            ensdim = ('ensemble',)
        for v_name, varin in ind.variables.items(): # Loop through input file variables
         if v_name == varn: # Replace sea-ice concentration variable with ifd/fud 
           dimsp=varin.dimensions
           if (dimx == 1):
              dims=('time',)+ensdim+dimsp[1:] 
           else:
              dims=dimsp    
           outVar = outnc.createVariable(var,varin.datatype, dims)
           outVar[:] = tfields[var]
           outVar.units = 'Julian Day'
           outVar.long_name = var  
         elif (v_name != timvar): # Copy any variables other than time from input file   
           outVar = outnc.createVariable(v_name, varin.datatype, varin.dimensions)
           outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})        
           outVar[:] =varin[:]
         else: # Create new time variable (years)
           outVar = outnc.createVariable('time','d',('time',)) 
           outVar.long_name = 'time'
           outVar.units = 'year'
           outVar.calendar = '365 day'
           outVar[:] = np.arange(firstyear, lastyear+1)
        outnc.creation_date = time.ctime(time.time())
        outnc.created_by = user_string+', git hash: '+hash
        outnc.close() 
    ###########################################################################
    #######WRITE NetCDF CLIMATLOGY FILES#######################################
    ###########################################################################
    ## Set up variables for ifd/fud climatologys, fractions defined, never-melted etc. 
    outncdct = { 'fud' : 'outnc_fud' , 'ifd' : 'outnc_ifd', 'fud_def' : 'outnc_fud_def', 'ifd_def' : 'outnc_ifd_def','aff':'outnc_aff','nff':'outnc_nff','amf':'outnc_amf','nmf':'outnc_nmf'  }             
    outfldct = { 'fud' : 'outfld_fud' , 'ifd' : 'outfld_ifd', 'fud_def' : 'outfld_fud_def', 'ifd_def' : 'outfld_ifd_def','aff':'outfld_aff','nff':'outfld_nff','amf':'outfld_amf','nmf':'outfld_nmf' }  
    cunits = {'fud' : 'Julian Day' , 'ifd' :  'Julian Day' , 'fud_def': 'fraction' , 'ifd_def': 'fraction', 'aff': 'fraction' , 'nff': 'fraction', 'amf': 'fraction' , 'nmf': 'fraction'  }
    cvariables = {'fud' : 'fudclim' , 'ifd' : 'ifdclim' , 'fud_def' : 'fud_def' , 'ifd_def' : 'ifd_def','aff':'aff','nff':'nff','amf':'amf','nmf':'nmf' }     
    cfields = {'fud' : fudclim , 'ifd' : ifdclim , 'fud_def' : fud_def, 'ifd_def' : ifd_def, 'aff':aff,'nff':nff,'amf':amf,'nmf':nmf } 
    cfile = {'fud' : 'fud' , 'ifd' : 'ifd' , 'fud_def' : 'fud', 'ifd_def' : 'ifd', 'aff':'fud','nff':'fud','amf':'ifd','nmf':'ifd' }  
    othervardt = {'fud': ('fud_def','aff','nff'),
              'ifd': ('ifd_def','amf','nmf')}
    for var in list(othervardt.keys()):
        outfile = '{}tn{}_{}_{}_{}{}_{}{}_clim_im{}_{}.nc'.format(outdir,str(threshold),str(nsd),cfile[var],str(firstyear),firstmonths,str(lastyear),lastmonths,imon_str,insuffix)
        outncdct[var] = Dataset(outfile,'w',format='NETCDF3_CLASSIC')
        for dname, the_dim in ind.dimensions.items():	
          outncdct[var].createDimension(dname, len(the_dim) if not the_dim.isunlimited() else None)
        for v_name, varin in ind.variables.items():
         if v_name == varn: # Fill sea ice variables 
           dims=varin.dimensions
           dimf=[element for i, element in enumerate(dims) if i in [dimx, dimy]] 
           outfldct[var] = outncdct[var].createVariable(cvariables[var],varin.datatype, dimf,fill_value=-999)
           outfldct[var].units = 'Julian Day'
           outfldct[var].long_name = var  
           outfldct[var].units = cunits[var]
           outfldct[var][:] = cfields[var]
         elif (v_name != timvar): # Copy other variables besides time 
           outVar = outncdct[var].createVariable(v_name, varin.datatype, varin.dimensions)
           outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})        
           outVar[:] = varin[:] 
        outncdct[var].creation_date = time.ctime(time.time())
        outncdct[var].created_by = user_string+', git hash: '+hash
        for othervar in othervardt[var]:
            outfldct[othervar] = outncdct[var].createVariable(cvariables[othervar],'d',dimf,fill_value=-999)
            outfldct[othervar].units = cunits[othervar]
            outfldct[othervar].long_name = othervar
            outfldct[othervar][:] = cfields[othervar]
# ############################
# PLOTS
# ############################
 if testplots=='on':
   for var in ['fud', 'ifd']:
        outncdct[var].close() 
        fig = plt.figure(figsize=(8,8))
        bm=rpl.make_bm(111,region='nps3',oceanmask=True,landmask=True,coastlinewidth=0.1)
        if var == 'ifd':
         pc=rpl.add_pc(bm,lon,lat,cfields[var],cmap='blue0grayred20',clevs=list(range(121,248,7)))
        if var == 'fud':
         pc=rpl.add_pc(bm,lon,lat,cfields[var],cmap='blue0grayred20',clevs=list(range(301,428,7)))
        rpl.add_cb(bm,pc)
        rpl.add_title(bm,title = var +' '+model+  ' initialized ' + imon_str) 
        plt.savefig(plotdir+var +'_'+model+'_'+ str(firstyear)+'_'+str(lastyear)+  '_initialized_' + imon_str)
       # plt.show() # Uncomment toshow plots interactively
   fig = plt.figure(figsize=(8,8))
   bm=rpl.make_bm(111,region='nps3',oceanmask=True,landmask=True,coastlinewidth=0.1)
   if var == 'ifd':
    pc=rpl.add_pc(bm,lon,lat,nmf,cmap='bluegrayred9dark',clevs=[.2,.3,.4,.5,.6,.7,.8,.9])
    rpl.add_title(bm,title = 'Fraction never melted '+model+  'initialized ' + imon_str) 
   if var == 'fud':
    pc=rpl.add_pc(bm,lon,lat,nff,cmap='bluegrayred9dark',clevs=[.2,.3,.4,.5,.6,.7,.8,.9])
    rpl.add_title(bm,title = 'Fraction never frozen '+model+  'initialized ' + imon_str)   
   rpl.add_cb(bm,pc)
  # plt.show() # Uncomment to show plots interactively





