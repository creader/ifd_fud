#!/usr/bin/env python
# PROGRAM: calc_acc_mse.py
# November, 2021: C. Reader: operational version
# July, 2020: C. Reader: Added user switch, threshold and nsd in filenames and directory, 
#             crudely added "use_model" switch to allow single-model skill to be calculated
#             (still reads both forecasts though...), cleaned up a bit.                           
# April 29/2018: C. Reader: Cleaned up and documented
# March 31/2018: C. Reader: Generalized for any suitably-defined grid
# Sept 26/2016: M. Sigmond: allows 2nd order polynomial trend removal
# April 22/2016: M.Sigmond/cleaned up (v144)
# NOV  16/2015: Cleaned-up and detrended MSE score added
# SEP 17/2015: C READER 
# 
# PURPOSE: Calculate and plot Anomaly corration Coefficients (raw and detrended) and mean-squared error skill 
#          between sea-ice forecast and obs. 
# 
# AUTHOR: Cathy Reader 
#  
# INPUT FILES: <basedir>/IFD_RAW/<model>/tn<threshold>_<nsd>_<var>_<firstyearf><firstmonthf>_<lastyearf><lastmonthf>_timeseries_im<month><minsuffix>.nc
#              <basedir>/IFD_RAW/<model>/tn<threshold>_<nsd>_<var>_<firstyearf><firstmonthf>_<lastyearf><lastmonthf>_clim_im<month><minsuffix>.nc
#              where <model> indicates forecast model name or "NASAmerged". 
# OUTPUT FILES: /DATA/SKILL<use_model>/skill_tn_<threshold>_<nsd>_<ifd/fud>_<model1>_model2>_<hc_period>_im<initmonth>_<minsuffix1>.nc'
#               <plots>
#                <bc ifds etc>
# ##############################################################################
# ###### IMPORT EXTERNAL MODULES ###############################################
# ##############################################################################  
import sys
import time  # used for putting timestamp on created files
import os.path # module for dealing with directories
import numpy as np                # import numpy to work with arrays/matrices
import matplotlib.pyplot as plt   # import matplotlib to do plotting
import numpy.ma as ma # masked arrays (imported separately to simplify code)
# netCDF4 package to manipulate netCDF files.
from netCDF4 import Dataset       # https://code.google.com/p/netcdf4-python/
import subprocess as sbp
# ##############################################################################
# BASIC USER-SET PARAMETERS:
# ##########################
# Directories:
user='mcr'  # Allows different setups for different users.
user_string='Cathy Reader, CCCma'
basedir=''
rundir=''
codedir=''
sys.path.append(codedir + 'MODS')
import nc as nc # Contains function to easily get netCDF variabbles 
import rms_plots as rpl
firstyearm=1981
lastyearm=2020 #first and last year in model ifd/fud files
firstmonthm=1
lastmonthm=12 #first and last month in model ifd/fud files
firstyearcs=1981
lastyearcs=2020 #first and last year to calculate skill
mon_init_list=np.array([10]) # Initialization months to process
#
# ###############################
# "ADVANCED" USER-SET PARAMETERS:
# ###############################
firstmonth=1
lastmonth=12 #first and last year to calculate skill - almost always full years.
mask_with_clim='on' # If on, mask where entire obs climatology has an always-melted or always frozen fraction >1-def_threshold
plots='on' # if 'on', make some plots
plotregions=False
nsd=10 #number of days to remain frozen or melted
threshold=.5 # Sea-ce concentration used to define freeze/melt 
use_model = 'b' # 1, 2 or b (for both)
polyorder=1 # Order of polynomial for trend removal (1 = linear or 2 = quadratic)
def_threshold = .75 # only plot points with defined values >= def_threshold
## Uncomment one of the 7-line sections below to run on old or new hindcasts/forecasts
## For other hindcasts/forecasts, create a corresponding set of input parameters
########### New Hindcasts
latvarname='latitude' #name of latitude variable in input files
lonvarname='longitude' #name of longitude variable in input files
minsuffix1='_1x1_grid' # model1 file suffix
minsuffix2='_NEW_1x1_grid' # model2 file suffix
oinsuffix='_filled_1_1x1' #obs file suffix
model1='GEM_NEMO' # indicator of first forecast 
model2='CanCM4' # indicator of first forecast
########### Old Hindcasts
#latvarname='latitude' #name of latitude variable in input files
#lonvarname='longitude' #name of longitude variable in input files
#minsuffix1='_CHFP_original_grid_i'
#minsuffix2='_CHFP_original_grid_i'
#oinsuffix='_filled_1_CanCM_masked'
#model1='CanCM3' # indicator of first forecast
#model2='CanCM4' # indicator of first forecast
# ########################################################################################
# PREPARATIONS
# ############
# record hash of current version
ghcmd='git rev-parse HEAD'
hash=sbp.check_output(["git","rev-parse","HEAD"])
hash=str(hash,'utf-8').strip('\n')
firstmonths= "%02d"%(firstmonth)
lastmonths= "%02d"%(lastmonth)
firstmonthms= "%02d"%(firstmonthm)
lastmonthms= "%02d"%(lastmonthm)
hc_period=str(firstyearcs)+firstmonths+'_'+str(lastyearcs)+lastmonths
hc_periodfm=str(firstyearm)+firstmonthms+'_'+str(lastyearm)+lastmonthms
hc_periodfo=str(firstyearm)+firstmonthms+'_'+str(lastyearm)+lastmonthms
imon_init_list=mon_init_list-1
imon_str0 = "%02d"%(mon_init_list[0])
td=str(threshold) +'_'+ str(nsd)
months=['Jan.','Feb.','Mar.','Apr.','May','Jun.','Jul.','Aug.','Sep.','Oct.','Nov.','Dec.']
## Make Directories
if plots=='on':
   plotdir = basedir +'PLOTS/SKILL'+use_model+'/' # plot directory
   if not os.path.isdir(plotdir):os.makedirs(plotdir)
outdir =  basedir +'DATA/SKILL'+use_model+'/' # output directory
if not os.path.isdir(outdir):os.makedirs(outdir)
indir=basedir+'DATA/IFD_RAW/'
lon=nc.getvar('{}NASAmerged/tn{}_ifd_{}_timeseries_im{}_filled_1_1x1.nc'.format(indir,td,hc_periodfo,imon_str0),'longitude')
lat=nc.getvar('{}NASAmerged/tn{}_ifd_{}_timeseries_im{}_filled_1_1x1.nc'.format(indir,td,hc_periodfo,imon_str0),'latitude')
#nt=len(nc.getvar('{}NASAmerged/tn{}_ifd_{}_timeseries_im{}_filled_1_1x1.nc'.format(indir,td,hc_periodfo,imon_str0),'time'))
ifd1=nc.getvar('{}NASAmerged/tn{}_ifd_{}_clim_im{}_filled_1_1x1.nc'.format(indir,td,hc_periodfo,imon_str0),'ifdclim')
years=np.arange(firstyearcs,lastyearcs+1) 
nt=len(years)
# dictionaries to define ice and ocean coloring for ifd and fud
nmfdct={'ifd':'nmf','fud':'aff'} 
amfdct={'ifd':'amf','fud':'nff'}
#FUNCTIONS###########################################################################
####calculate root-mean-squared error################################################
def calc_rmse(predictions, targets,nyears): 
        """
        rmse(predictions,targets,nyears)
        Inputs: predictions: timeseries of annual forecast arrays
                targets: timeseries of annual obs. arrays
                nyears: number of years
        Returns: array of root-mean-squared-error skill scores                
        """
        ## Calculate root-mean-squared-error skill. For detrended points that were constant to start with,
        ## set rmse skill to zero rather than the tiny value resulting from numerical effects of detrending           
        if  np.sqrt(((predictions - targets) ** 2).mean()/float(nyears)) >1e-10:
         return np.sqrt(((predictions - targets) ** 2).mean()/float(nyears))      
        else:
         return 0.   
####add_regions####################################################################
def add_regions(bm,lat,lon,x):
    lons, lats = np.meshgrid(lon,lat)  
    nlon=len(lon);nlat=len(lat);
    for ireg in [1,2,3,4,5,6,7,8,9,10,11,13,14]:
        y=np.zeros([nlat,nlon]);
        y[region==ireg]=1    
        bm.contour(lons,lats,y,colors='g',linewidths=1,latlon=True,levels=[0.5])

####add_scatter####################################################################
#def add_scatter(bm,lat,lon,x):
#    nlon=len(lon);nlat=len(lat);
#    cols=np.zeros([nlat,nlon]);size=np.zeros([nlat,nlon])
#    cols[np.logical_and(x>=0.75,x<1)]=1
#    size[np.logical_and(x>=0.75,x<1)]=1
#    lons, lats = np.meshgrid(lon,lat)  
#    colors=np.array([ [1,1,1],[0,0,0]],dtype=float); clevs=[1.5]         
#    cmaploc, norm = from_levels_and_colors(clevs, colors ,extend='both')
#    bm.scatter(lons,lats,latlon=True,c=cols, s=size, cmap=cmaploc,norm=norm,edgecolors='None') 
####add_ice####################################################################
def add_ice(map,lat,lon,fice):
    colors=np.array([ [0,0,0],[1,1,1]],dtype=float);clevs=[0]
    rpl.add_plot_latlon(lat,lon,np.ma.masked_where(fice<=0.75,fice),map,clevs=clevs,colors=colors,
                        shadetype='pcolor',supp_cb=True)
#### plot one subplot #######################################
def plot_skill_one_month(sbpt,lat,lon,x,f,fice,titlepanel,plotregions=False):
    ##colors and clevs
    full_cmap = plt.get_cmap('bluegrayred9dark')
    colors=full_cmap([1,4,5,6,7,8])    
    clevs=np.array([0.0000001,0.3,0.5,0.7,0.9])
    ##plot
    bm=rpl.make_basemap(maptype='nsnhz',sbpt=sbpt) 
    pc, cmap, norm =rpl.add_plot_latlon(lat,lon,np.ma.masked_where(f<0.25,x),bm,axis=bm,clevs=clevs,supp_cb=True,colors=colors,shadetype='pcolor')
#    #add_scatter(bm,lat,lon,f)
    pc=add_ice(bm,lat,lon,fice)
    pc=rpl.add_title(bm,titlepanel)
    if plotregions: add_regions(bm,lat,lon,region)
    return pc, cmap, norm 
#### 2-panel plot #######################################
def plot_skill_one_month_2panels(lat,lon,x,xdt,f,fice,ifdfud,imon):
    imonstr="%02d" % (imon+1)
    ##open fig
    fig=plt.figure(figsize=(4,3))
    fig.subplots_adjust(left=0.1,right=0.9,top=.85,bottom=0.1,wspace=0.05,hspace=0.05)
    sbpt=[121,122]
    ###panels A-C
    pc, cmap, norm = plot_skill_one_month(sbpt[0],lat,lon,np.squeeze(x[imon,:,:]),np.squeeze(f[imon,:,:]),np.squeeze(fice[imon,:,:]),titlepanel='Total',plotregions=plotregions)
    pc, cmap, norm = plot_skill_one_month(sbpt[1],lat,lon,np.squeeze(xdt[imon,:,:]),np.squeeze(f[imon,:,:]),np.squeeze(fice[imon,:,:]),titlepanel='Detrended',plotregions=plotregions)
    ##title
    fig.suptitle('{} ACC ({} initialization)'.format(ifdfud,months[imon]),fontsize=14,y=1)
    ##cbar 
    cbar_ax = fig.add_axes([0.91,0.1,0.015,0.75])
    sm = plt.cm.ScalarMappable(cmap=cmap,norm=norm)
    sm._A = []
    cbar=fig.colorbar(sm,cax=cbar_ax,orientation='vertical',extendfrac='auto',spacing='proportional')
    cbar.ax.set_yticklabels(['0.0','0.3','0.5','0.7','0.9'])
    cbar.ax.tick_params(labelsize=8) 
    ##save
#    plt.show() # Uncomment to show plot interactively
    fig.savefig(plotdir+'tn{}_{}_acc_{}_im{}.png'.format(td,ifdfud,hc_period,imonstr),dpi=400)
##############################################################################################################
#Loop over initialization months
for imon in imon_init_list:     
    imon_str = "%02d"%(imon+1)
    # Loop over variables
    for var in ['ifd','fud']:
        # set up arrays
        varo = np.zeros((nt,ifd1.shape[0],ifd1.shape[1]));   varo_dt = np.zeros((nt,ifd1.shape[0],ifd1.shape[1])) 
        varm_em = np.zeros((nt,ifd1.shape[0],ifd1.shape[1]));varm_em_dt = np.zeros( (nt,ifd1.shape[0],ifd1.shape[1]) ); 
        obslope = np.zeros( (ifd1.shape[0],ifd1.shape[1]) ); moslope = np.zeros( (ifd1.shape[0],ifd1.shape[1]) )
        #
        # Read in timeseries from 2 models and obs+'_'+str(nsd)
        file3 = '{}{}/tn{}_{}_{}_timeseries_im{}{}.nc'.format(indir,model1,td,var,hc_periodfm,imon_str,minsuffix1)
        file4 = '{}{}/tn{}_{}_{}_timeseries_im{}{}.nc'.format(indir,model2,td,var,hc_periodfm,imon_str,minsuffix2)
        fileo = '{}NASAmerged/tn{}_{}_{}_timeseries_im{}{}.nc'.format(indir,td,var,hc_periodfo,imon_str,oinsuffix)
        var1 = nc.getvar(file3,var)[firstyearcs-firstyearm:lastyearcs-firstyearm+1]
        var2 = nc.getvar(file4,var)[firstyearcs-firstyearm:lastyearcs-firstyearm+1]
        varo = nc.getvar(fileo,var)[firstyearcs-firstyearm:lastyearcs-firstyearm+1].squeeze()
#        # if desired, make masks of always melted/frozen fractons - currently uses entire climatology to mask, not firstyear-lastyear.
#        nmf=nc.getvar('{}NASAmerged/tn{}_{}_{}_clim_im{}{}.nc'.format(indir,td,var,hc_periodfo,imon_str,oinsuffix),nmfdct[var])
#        amf=nc.getvar('{}NASAmerged/tn{}_{}_{}_clim_im{}{}.nc'.format(indir,td,var,hc_periodfo,imon_str,oinsuffix),amfdct[var])  
#        icemask=np.zeros((amf.shape))
#        watermask=np.zeros((amf.shape))        
#        if mask_with_clim='on':
#          icemask[nmf>1.-def_threshold]=1
#          watermask[amf>1.-def_threshold]=1
        # Choose or combine forecasts
        if (use_model == 'b'):
         print('using both models')   
         varm = (var1 + var2)/2. 
        elif (use_model == '1') :
         print('using model1')   
         varm = var1
        else:
         print('using model2')   
         varm = var2   
        varm_em=np.mean(varm,axis=1).squeeze()          
        # Bias correct
        varm_em_bc = varm_em - np.mean(varm_em,axis=0) + np.mean(varo,axis=0)       
        # Detrend
        for i in range(ifd1.shape[0]):
             for j in range(ifd1.shape[1]):     
                varo_dt[:,i,j]    = varo[:,i,j]    - np.poly1d(np.polyfit(years,np.squeeze(varo[:,i,j]),polyorder))(years)
                varm_em_dt[:,i,j] = varm_em[:,i,j] - np.poly1d(np.polyfit(years,np.squeeze(varm_em[:,i,j]),polyorder))(years)
        # Bias-correct detrended forecast 
        varm_em_bc_dt = varm_em_dt - np.mean(varm_em_dt,axis=0) + np.mean(varo_dt,axis=0)
        ## CORRELATION AND RMSE ###############################################
        #Initialize arrays
        corr = np.zeros((ifd1.shape[0],ifd1.shape[1])); corr_dt = np.zeros((ifd1.shape[0],ifd1.shape[1]))
        rmse_bc = np.zeros( (ifd1.shape[0],ifd1.shape[1]) ); rmse_bc_dt = np.zeros( (ifd1.shape[0],ifd1.shape[1]) )
        rmse_clim = np.zeros((ifd1.shape[0],ifd1.shape[1])); rmse_clim_dt = np.zeros((ifd1.shape[0],ifd1.shape[1]))
        mse_skill = np.zeros((ifd1.shape[0],ifd1.shape[1])); mse_skill_dt = np.zeros((ifd1.shape[0],ifd1.shape[1]))  
        # loop over points 
        for i in range(ifd1.shape[0]): 
             for j in range(ifd1.shape[1]):
                #ACC     
                #Set ACC effectively constant points to nan 
                if (np.std(varm_em[:,i,j]) <1e-8) and (np.std(varo[:,i,j])<1e-8):   
                  corr[i,j]=np.nan
                  corr_dt[i,j]=np.nan
                else:       
                 corr[i,j]=np.corrcoef(varo[:,i,j],varm_em[:,i,j])[0,1] # ACC
                 corr_dt[i,j]=np.corrcoef(varo_dt[:,i,j],varm_em_dt[:,i,j])[0,1] # detrended ACC
                #RMSE
                rmse_bc[i,j]      = calc_rmse(varm_em_bc[:,i,j],      varo[:,i,j],nt) # RMSE of bias corrected forecast
                rmse_clim[i,j]    = calc_rmse(np.mean(varo[:,i,j],axis=0),   varo[:,i,j],nt) # RMSE of climatology
                mse_skill[i,j]    = 1. - rmse_bc[i,j]/rmse_clim[i,j] # MSE skill score      
                rmse_bc_dt[i,j]   = calc_rmse(varm_em_bc_dt[:,i,j],   varo_dt[:,i,j],nt) # RMSE of bias corrected forecast (detrended)
                rmse_clim_dt[i,j] = calc_rmse(np.mean(varo_dt[:,i,j],axis=0),varo_dt[:,i,j],nt) # RMSE of climatology (detrended)               
                mse_skill_dt[i,j] = 1. - rmse_bc_dt[i,j]/rmse_clim_dt[i,j] # MSE skill (detrended)
        ##############################################################################
        # OUTPUT SKILL
        ##############################################################################  
        # open file
        sfile='skill_tn{}_{}_{}_{}_{}_im{}{}.nc'.format(td,var,model1,model2,hc_period,imon_str,minsuffix1)
        outnc = Dataset(outdir+sfile,'w',format='NETCDF3_CLASSIC')
        ind=Dataset(indir+ 'NASAmerged/tn'+td+'_'+var+'_'+hc_periodfo+'_clim_im'+imon_str+oinsuffix+'.nc','r') # use input clim file for variable info
        for dname, the_dim in ind.dimensions.items():# copy dimensions from input file
          outnc.createDimension(dname, len(the_dim) if not the_dim.isunlimited() else None)
        for v_name, varin in ind.variables.items(): 
          try:
           if ind.variables[v_name].standard_name=='latitude' : # copy latitude variable  
              outVar = outnc.createVariable(v_name, varin.datatype, varin.dimensions)
              outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})                                                             
              outVar[:] =varin[:]
          except:
           pass
          try:
           if ind.variables[v_name].standard_name=='longitude': # copy longitude variable 
              outVar = outnc.createVariable(v_name, varin.datatype, varin.dimensions)
              outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})                                                             
              outVar[:] =varin[:]
          except:
           pass   
          if v_name==var+'clim': # use climatology variable as model to fill in skill variables
              dims=varin.dimensions
              ofldx =  {var + 'rmse_skill': 'ofld1',   var + 'rmse_skill_dt': 'ofld2',      var + 'corr': 'ofld3', var + 'corr_dt' : 'ofld4', var + 'obstr' : 'ofld5', var + 'modtr': 'ofld6' } 
              ofields = {var + 'rmse_skill': mse_skill,var + 'rmse_skill_dt': mse_skill_dt, var + 'corr': corr,    var + 'corr_dt' : corr_dt, var + 'obstr' : obslope, var + 'modtr': moslope } 
              for vari in [var + 'corr', var + 'corr_dt' , var + 'obstr' ,var + 'modtr',var + 'rmse_skill', var + 'rmse_skill_dt']: #Loop through skill variables
                ofldx[vari]= outnc.createVariable(vari,'d',(dims))
                ofldx[vari][:] = ofields[vari]     
        outnc.creation_date = time.ctime(time.time())
        outnc.created_by = user_string+', git hash: '+hash
        outnc.close() 
#############################################################       
# PLOTS
# ########
if plots=='on':
   lon=nc.getvar(basedir +'DATA/SKILL{}/skill_tn{}_ifd_GEM_NEMO_CanCM4_{}_im{}{}.nc'.format(use_model,td,hc_period,imon_str0,minsuffix1),'longitude')
   lat=nc.getvar(basedir +'DATA/SKILL{}/skill_tn{}_ifd_GEM_NEMO_CanCM4_{}_im{}{}.nc'.format(use_model,td,hc_period,imon_str0,minsuffix1),'latitude')
   nlon=len(lon);nlat=len(lat)
##Regions
   if plotregions: region=nc.getvar(basedir+'DATA/IN_AUX_FILES/iceregions_128_64.nc','REG').squeeze()[-1:-19:-1,:]
###initialization 
##ACC, ACC_dt, ACC_pers, ACC_pers_dt, fraction defined and fraction ice and clim
   fud_acc=np.zeros((12,nlat,nlon));        ifd_acc=np.zeros((12,nlat,nlon));      
   fud_accdt=np.zeros((12,nlat,nlon));      ifd_accdt=np.zeros((12,nlat,nlon));
   fud_f=np.zeros((12,nlat,nlon));          ifd_f=np.zeros((12,nlat,nlon)); 
   fud_fice=np.zeros((12,nlat,nlon));       ifd_fice=np.zeros((12,nlat,nlon))  
###read
   loc_fld_dct={'fud_acc':fud_acc, 
             'ifd_acc':ifd_acc, 
             'fud_accdt':fud_accdt,
             'ifd_accdt':ifd_accdt,
             'fud_f':fud_f,
             'ifd_f':ifd_f,
             'fud_fice':fud_fice,
             'ifd_fice':ifd_fice,
             }
   nc_fld_dct={'fud_acc':'fudcorr',
            'ifd_acc':'ifdcorr',
            'fud_accdt':'fudcorr_dt',
            'ifd_accdt':'ifdcorr_dt',
           'fud_f':'fud_def',
            'ifd_f':'ifd_def',
            'fud_fice':'aff',
            'ifd_fice':'nmf',
             }

   for imon in imon_init_list: 
    imonstr="%02d" % (imon+1)
    nc_file_dct={'fud_acc':'SKILL{}/skill_tn{}_fud_GEM_NEMO_CanCM4_{}_im{}{}.nc'.format(use_model,td,hc_period,imonstr,minsuffix1),
                 'ifd_acc':'SKILL{}/skill_tn{}_ifd_GEM_NEMO_CanCM4_{}_im{}{}.nc'.format(use_model,td,hc_period,imonstr,minsuffix1),
                 'fud_accdt':'SKILL{}/skill_tn{}_fud_GEM_NEMO_CanCM4_{}_im{}{}.nc'.format(use_model,td,hc_period,imonstr,minsuffix1),
                 'ifd_accdt':'SKILL{}/skill_tn{}_ifd_GEM_NEMO_CanCM4_{}_im{}{}.nc'.format(use_model,td,hc_period,imonstr,minsuffix1),
                     'fud_f'   : 'IFD_RAW/NASAmerged/tn{}_fud_{}_clim_im{}{}.nc'.format(td,hc_periodfo,imonstr,oinsuffix),
                     'ifd_f'   : 'IFD_RAW/NASAmerged/tn{}_ifd_{}_clim_im{}{}.nc'.format(td,hc_periodfo,imonstr,oinsuffix),
                     'fud_fice': 'IFD_RAW/NASAmerged/tn{}_fud_{}_clim_im{}{}.nc'.format(td,hc_periodfo,imonstr,oinsuffix),
                     'ifd_fice': 'IFD_RAW/NASAmerged/tn{}_ifd_{}_clim_im{}{}.nc'.format(td,hc_periodfo,imonstr,oinsuffix)
                 }
    for var in list(loc_fld_dct.keys()):
           loc_fld_dct[var][imon,:,:]=nc.getvar(basedir+'DATA/'+nc_file_dct[var],nc_fld_dct[var])
    if mask_with_clim != 'on':
           fud_f[...]=1.
           fud_fice[...]=0.
           ifd_f[...]=1.
           ifd_fice[...]=0.           
    plot_skill_one_month_2panels(lat,lon,fud_acc,fud_accdt,fud_f,fud_fice,'fud',imon)
    plot_skill_one_month_2panels(lat,lon,ifd_acc,ifd_accdt,ifd_f,ifd_fice,'ifd',imon)    



