# PROGRAM: calc+plot_fc.py
# PURPOSE: Calculate and plot forecast and observed IFD/FUD and 
#          write Bias corrected 2 model mean forecast to Netcdf file for submisssion to
#
# AUTHOR: Michael Sigmond (CCCma), July 6, 2018, based on plot_ifd_fud.py by Cathy Reader
# Operational version: Cathy Reader, November 2021.
#
# INPUT: obs. and model ifd/fud timeseries: <basedir>DATA/IFD_RAW/<model>/<td>_<var>_<period>_timeseries_im<init_month>_<suffix>.nc
# where <model>=NASAmerged, CanCM4 or GEM_NEMO, <period>=<firstyear><firstmonth>-<lastyear><lastmonth> for "model"
# (i.e. forecasts go farther than obs.) 
# if apply_alreadymask  : <basedir>DATA/IN_DAILY_SIC_OBS/MERGED/seaice_conc_daily_nh_cdr_seaice_conc_<period>_nrt_<nrt_time>_<insuffix> 
#   
# OUTPUT: raw forecast   : PLOTS/{}_tn{}_fc_i{}m{}_raw.png    
#         biascor. forecst: PLOTS/{}_tn{}_fc_i{}m{}_bc_cf_{}.png
#         forecast anomaly: PLOTS/{}_tn{}_fc_i{}m{}_anom_cf_{}.png    
#         biascor. forecst: DATA/{}_tn{}_bc_cf_{}_i{}m{}_CanSIPSv2.nc
#
##############################################################################
#IMPORT EXTERNAL MODULES #####################################################
##############################################################################
import os.path # module for dealing with directories
import numpy as np # python scientific computing package
import numpy.ma as ma # masked arrays (imported separately to simplify code) 
import cartopy.crs as ccrs
import matplotlib.pyplot as plt # basic plotting module
from netCDF4 import Dataset
import subprocess as sbp
import time
import sys
# ##############################################################################
# BASIC USER-SET PARAMETERS:
# ##########################
fc_year_init=2021; 
mon_init_list=[10]
base_period_list=['2012_2020'] # the period relative to which anomalies are calculated and the period used for bias correction                                                  
hc_period='1981_2020'; #the years to consider for hindcast
firstyearm=1981
lastyearm=2021 #first and last year in model ifd/fud files
firstmonthm=1
lastmonthm=12 #first and last month in model ifd/fud files
firstyearo=1981
lastyearo=2020 # first and last year in obs ifd/fud files
firstmontho=1
lastmontho=12 # first and last month in obs ifd/fud files
nrt_year=2021 # starting year of near-real-time data
nrt_month=1 # starting year of near-real-time data
#user switch for directory locations
user_string=''
basedir='/HOME/mcr/new_IFD/'
rundir='/HOME/mcr/new_IFD/tmp/'
codedir='/HOME/mcr/SIC_operationalization/git_ifd/'
varlist=['fud']
verify=False # if True then observations are available for verification
#########################################################################################################
## "ADVANCED" SETTINGS:
#######################
testplot=False
tlati=-5
tloni=34 # If tesplot==True, plots a timeseries at a test point index [tlati,tloni]
with_majority_filter=True #if True, set bc event never to occur (maxval) if majority of bc members predict that
threshold=0.5
nsd=10; # ysea-ice concentration threshold and number of days required to stay melted/frozen
mod1='GEM_NEMO'
mod2='CanCM4'
alreadymask_override=False # override the automatic use of obs-based mask if verification data is available
osuffix='filled_1_1x1' # suffix of obs. files
##########################################################################################################
# PREPARATIONS:
###############
# record hash of current version
ghcmd='git rev-parse HEAD'
hash=sbp.check_output(["git","rev-parse","HEAD"])
hash=str(hash,'utf-8').strip('\n')
nrt_months="%02d" % (nrt_month)
nrt_time=str(nrt_year)+nrt_months   
if verify == True and alreadymask_override==False: 
  alreadymask_available=True #if True, mask out forecasts in locations where event has already occured based on
else:                         # observations on the last day of previous month (instead of SIC of model)
  alreadymask_available=False
# Directories  
indir=basedir+'DATA/IFD_RAW/'
plotdir=basedir+'PLOTS/'
datadir=basedir+'DATA/IFD_BIASCOR/'
if not os.path.isdir(datadir):os.makedirs(datadir) 
if not os.path.isdir(plotdir):os.makedirs(plotdir) 
if not os.path.isdir(plotdir+'2mods'):os.makedirs(plotdir+'2mods')
sys.path.append(codedir + 'MODS')
import rms_plots as rpl # plotting routines, original rms_plots by Michael Sigmond
import rms_utils as rut
import nc as nc # module to deal with netCDF files
#strings
td=str(threshold) +'_'+ str(nsd)
fc_year_inits=str(fc_year_init)
fmos="%02d"%(firstmontho);lmos="%02d"%(lastmontho);fmms="%02d"%(firstmonthm);lmms="%02d"%(lastmonthm)
hc_years=np.arange(np.int(hc_period[0:4]),np.int(hc_period[5::])+1)
fm_period=str(firstyearm)+fmms+'_'+str(lastyearm)+lmms
fo_period=str(firstyearo)+fmos+'_'+str(lastyearo)+lmos
# last day of month
ldm = np.array([31,28,31,30,31,30,31,31,30,31,30,31])
# julian day of month        
jdm = np.array([1,  32,60,91,121,152,182,213,244,274,305,335,
                366,397, 425, 456, 486, 517, 547, 578, 609, 639, 670, 700])  
#julian day of halve month and full month        
jdm1=np.zeros(24*2)
for i in range(0,48,2):jdm1[i]=jdm[int(i/2)]
for i in range(1,47,2):jdm1[i]=(jdm1[i+1]+jdm1[i-1])/2;jdm1[47]=(jdm1[46]+731.)/2.
###########################################################################################################
for mon_init in mon_init_list:###loop over imons
  mon_inits="%02d"%(mon_init);imon=mon_init-1;imons="%02d"%(imon)
  print('month_init:' + mon_inits)
  # get lats and lons
  try:
      lon=nc.getvar('{}CanCM4/tn{}_ifd_{}_timeseries_im{}_NEW_1x1_grid.nc'.format(indir,td,fm_period,mon_inits),'longitude')
      lat=nc.getvar('{}CanCM4/tn{}_ifd_{}_timeseries_im{}_NEW_1x1_grid.nc'.format(indir,td,fm_period,mon_inits),'latitude')
  except:
      lon=nc.getvar('{}CanCM4/tn{}_ifd_{}_timeseries_im{}_NEW_1x1_grid.nc'.format(indir,td,fm_period,mon_inits),'lon')
      lat=nc.getvar('{}CanCM4/tn{}_ifd_{}_timeseries_im{}_NEW_1x1_grid.nc'.format(indir,td,fm_period,mon_inits),'lat')    
    
  for var in varlist: # Loop over variables
    if var=='fud' and mon_init in [10,11,12,1,2,3] and alreadymask_available: apply_alreadymask=True
    elif var=='ifd' and mon_init in [4,5,6,7,8,9] and alreadymask_available: apply_alreadymask=True
    else: apply_alreadymask=False
    print('var: '+var)
    if var=='ifd':
        # day before start meltseason (1 Apr)
        var_min = np.array([90,90,90,90,90,90,90,90,455,455,455,455]) # setting for plotting operational forecasts: from im09 onward: look at next year
        #end melt season (1 Oct)
        var_max = np.array([273,273,273,273,273,273,273,273,608,638,638,638]) # setting for plotting operational forecasts: from im09 onward: look at next year
    if var=='fud':
        #day before start freeze season (1 Oct)
        var_min = np.array([273,273,273,273,273,273,273,273,273,273,273,273]) 
        #end freeze season (1 Apr), or last day of forecast range
        var_max = np.array([365,396,424,455,455,455,455,455,455,455,455,455]) 
    ########################################################################################
    ############################################## read data ###############################
    ########################################################################################
    ##obs + hindcast timeseries
    fnamebase='tn{}_{}_{}'.format(td,var,fo_period)
    obs_ts=nc.getvar('{}NASAmerged/{}_timeseries_im{}_filled_1_1x1.nc'.format(indir,fnamebase,mon_inits),var).squeeze()[hc_years[0]-firstyearo:hc_years[-1]-firstyearo+1]
    fnamebase='tn{}_{}_{}'.format(td,var,fm_period)
    hc1_ts=nc.getvar('{}{}/{}_timeseries_im{}_1x1_grid.nc'.format(indir,mod1,fnamebase,mon_inits),var)[hc_years[0]-firstyearm:hc_years[-1]-firstyearm+1]
    hc2_ts=nc.getvar('{}{}/{}_timeseries_im{}_NEW_1x1_grid.nc'.format(indir,mod2,fnamebase,mon_inits),var)[hc_years[0]-firstyearm:hc_years[-1]-firstyearm+1]
    ##forecasts
    fnamebase='tn{}_{}_{}'.format(td,var,fm_period)    
    fc1=nc.getvar('{}{}/{}_timeseries_im{}_1x1_grid.nc'.format(indir,mod1,fnamebase,mon_inits),var)[fc_year_init-firstyearm:fc_year_init-firstyearm+1].squeeze()
    fc2=nc.getvar('{}{}/{}_timeseries_im{}_NEW_1x1_grid.nc'.format(indir,mod2,fnamebase,mon_inits),var)[fc_year_init-firstyearm:fc_year_init-firstyearm+1].squeeze()
    if verify:
        fnamebase='tn{}_{}_{}'.format(td,var,fo_period)
        obs=nc.getvar('{}NASAmerged/{}_timeseries_im{}_filled_1_1x1.nc'.format(indir,fnamebase,mon_inits),var)[fc_year_init-firstyearm:fc_year_init-firstyearm+1].squeeze()
    ##read sic for alreadymask
    if apply_alreadymask:
      cmds='cdo seldate,'+str(fc_year_init)+'-'+imons+'-'+str(ldm[imon-1])+' {}DATA/IN_DAILY_SIC_OBS/MERGED/seaice_conc_daily_nh_cdr_seaice_conc_{}{}_{}{}_nrt_{}_{}.nc '.format(basedir,firstyearm,fmos,lastyearm,lmos,nrt_time,osuffix)+ rundir + 'sicmask'
      sbp.call(cmds,shell=True)
      sic=nc.getvar(rundir+'sicmask','cdr_seaice_conc').squeeze()
      sbp.call('rm '+rundir+'sicmask',shell=True)
    ########################################################################################
    ############################################## Calculations ############################
    ########################################################################################
    ######################################### fc_raw #######################################
    fc1_em=np.mean(fc1,axis=0)
    fc2_em=np.mean(fc2,axis=0)
    fc12_em=(fc1_em+fc2_em)/2.
    #
    for base_period in base_period_list: ## loop over baseperiod (for calculating anomalies and bias corrected values)
        base_year_start=np.int(base_period[0:4]); base_year_stop=np.int(base_period[5::])
        iyear_start=rut.find_nearest(hc_years,base_year_start); iyear_stop=rut.find_nearest(hc_years,base_year_stop)
        #########################################clims######################################
        ## STEP 0: calculate climatologies
        obs_clim=np.mean(obs_ts[iyear_start:iyear_stop+1],axis=0).squeeze()
        hc1_clim=np.mean(np.mean(hc1_ts[iyear_start:iyear_stop+1,...],axis=0),axis=0)
        hc2_clim=np.mean(np.mean(hc2_ts[iyear_start:iyear_stop+1,...],axis=0),axis=0)
        hc_clim12=(hc1_clim+hc2_clim)/2.
        #########################################fc_bias_corrected##########################
        ## STEP 1: subtract mean bias
        fc1_bc= np.zeros_like(fc1);fc2_bc=np.zeros_like(fc1)
        for i in range(10): 
          fc1_bc[i,...]=fc1[i,...]-(hc1_clim-obs_clim);
          fc2_bc[i,...]=fc2[i,...]-(hc2_clim-obs_clim);     
        fc1_em_bc_ma=np.mean(fc1_bc,axis=0)
        fc2_em_bc_ma=np.mean(fc2_bc,axis=0)
        fc12_em_bc_ma=(fc1_em_bc_ma+fc2_em_bc_ma)/2.
        # var set to minval if event not occured at start, 
        # var set to maxval if event not predicted to occur before end run or retreat/advance season
        minval=max(var_min[imon],jdm[imon]-1)
        maxval=var_max[imon]

        ## STEP 2: Set to already melted/frozen point to minval, based on observational data on day before initilaization
        ##          (if available) or based on what model sees (if raw data=minval)
        ##          The first option is better as model not always properly initialized (due to assimilation or mismatch assimilation/obs datasets)
        if apply_alreadymask: # set already melted/frozen point to minval, based on observational data on day before initilaization
          if var=='ifd':
            fc1_em_bc_ma[(sic<threshold)]=minval
            fc2_em_bc_ma[(sic<threshold)]=minval
            fc12_em_bc_ma[(sic<threshold)]=minval
          if var=='fud':
            fc1_em_bc_ma[(sic>threshold)]=minval
            fc2_em_bc_ma[(sic>threshold)]=minval
            fc12_em_bc_ma[(sic>threshold)]=minval
        else: #set already melted/frozen point to minval, based on what model sees (i.e. if raw data=minval)
          fc1_em_bc_ma[(fc1_em==minval)]=minval
          fc2_em_bc_ma[(fc2_em==minval)]=minval
          fc12_em_bc_ma[(fc12_em==minval)]=minval
        ## STEP 3: set to minimum if BC put it below that value, and to maximum if BC put it above that value
        ## NB: had to 'fill' the masked arrays, otherwise you lose the mask in the 2nd and 3rd line below
        fc1_em_bc=fc1_em_bc_ma.filled(np.nan); fc2_em_bc=fc2_em_bc_ma.filled(np.nan); fc12_em_bc=fc12_em_bc_ma.filled(np.nan)
        fc1_em_bc[fc1_em_bc<minval]=minval; fc2_em_bc[fc2_em_bc<minval]=minval; fc12_em_bc[fc12_em_bc<minval]=minval
        fc1_em_bc[fc1_em_bc>maxval]=maxval; fc2_em_bc[fc2_em_bc>maxval]=maxval; fc12_em_bc[fc12_em_bc>maxval]=maxval 
        ## STEP 4: set back to maximum value if >50% of ensemble members predict that event won't occur        
        if with_majority_filter:
          fc1_em_bc[np.sum(fc1_bc>=maxval,axis=0)>5]=maxval 
          fc2_em_bc[np.sum(fc2_bc>=maxval,axis=0)>5]=maxval 
          fc12_em_bc[(np.sum(fc1_bc>=maxval,axis=0)+np.sum(fc2_bc>=maxval,axis=0))>10]=maxval 
        #########################################fc_anomalies###############################
        ## STEP 1: subtract mean climatologies
        fc1_anom= np.zeros_like(fc1);fc2_anom=np.zeros_like(fc1)
        for i in range(10): 
          fc1_anom[i,...]=fc1[i,...]-hc1_clim;
          fc2_anom[i,...]=fc2[i,...]-hc2_clim     
        fc1_em_anom=np.mean(fc1_anom,axis=0); fc2_em_anom=np.mean(fc2_anom,axis=0); fc12_em_anom=(fc1_em_anom+fc2_em_anom)/2.
        ## STEP 2: Mask out already melted/frozen points based on observations
        ##          (if available) or based on what model sees (if raw data=minval)
        ##          The first option is better as model not always properly initialized (due to assimilation or mismatch assimilation/obs datasets)
        if apply_alreadymask: # set already melted/frozen point to minval, based on observational data on day before initilaization
          if var=='ifd':
            fc1_em_anom=ma.masked_where((sic<threshold),fc1_em_anom)
            fc2_em_anom=ma.masked_where((sic<threshold),fc2_em_anom)
            fc12_em_anom=ma.masked_where((sic<threshold),fc12_em_anom)
          if var=='fud':
            fc1_em_anom=ma.masked_where((sic>threshold),fc1_em_anom)
            fc2_em_anom=ma.masked_where((sic>threshold),fc2_em_anom)
            fc12_em_anom=ma.masked_where((sic>threshold),fc12_em_anom)
        else: #set already melted/frozen point to minval, based on what model sees (i.e. if raw data=minval)
          fc1_em_anom=ma.masked_where((fc1_em==minval),fc1_em_anom)
          fc2_em_anom=ma.masked_where((fc2_em==minval),fc2_em_anom)
          fc12_em_anom=ma.masked_where((fc12_em==minval),fc12_em_anom)
        ## STEP 3: mask out where event does not happen in BC data
        fc1_em_anom=ma.masked_where((fc1_em_bc==var_max[imon]),fc1_em_anom)
        fc2_em_anom=ma.masked_where((fc2_em_bc==var_max[imon]),fc2_em_anom)
        fc12_em_anom=ma.masked_where((fc12_em_bc==var_max[imon]),fc12_em_anom)              
        #########################################obs_anomalies##############################
        if verify:
            ## STEP 1: subtract mean climatologies
            obs_anom=obs-obs_clim;
            ## STEP 2: Mask out already melted/frozen points 
            ##          (if available) or based on what model sees (if raw data=minval)
            ##          The first option is better as model not always properly initialized (due to assimilation or mismatch assimilation/obs datasets)
            if apply_alreadymask: # set already melted/frozen point to minval, based on observational data on day before initilaization
              if var=='ifd':
                obs_anom=ma.masked_where((sic<threshold),obs_anom) 
              if var=='fud':
                obs_anom=ma.masked_where((sic>threshold),obs_anom)
              obs_anom=ma.masked_where((obs_anom==0),obs_anom)  ## this step is necessary to mask out gridpoints that are already frozen/melted or never freeze/melt
            else: #set already melted/frozen point to minval, based on what model sees (i.e. if raw data=minval)
              obs_anom=ma.masked_where((fc12_em==minval),obs_anom)
            ## STEP 3: Mask out where event does not happen
              obs_anom=ma.masked_where((obs>=maxval),obs_anom)
        ###############################################################################
        #######PLOT####################################################################
        ###############################################################################
        #######RAW+BIAS_CORRECTED###########################################################
        ## colors, levels
        cmapj='blue0grayred20'
        if var=='ifd':
          if mon_init<=4:cmapcb=[10,19,17,15,13,12,11,8,7,6,4,2,0,9]
          if mon_init==5:cmapcb=[10,10,10,15,13,12,11,8,7,6,4,2,0,9]
          if mon_init==6:cmapcb=[10,10,10,10,10,12,11,8,7,6,4,2,0,9]
          if mon_init==7:cmapcb=[10,10,10,10,10,10,10,8,7,6,4,2,0,9]
          if mon_init>=8:cmapcb=[10,10,10,10,10,10,10,10,10,6,4,2,0,9]
          #      
          clevsi = np.append(jdm1[6:17+1],jdm1[18]-1)
          cbticks= np.append(jdm[3:8+1],jdm[9]-1)  
          cbticklabels=['Apr 1', 'May 1', 'June 1','Jul 1','Aug 1', 'Sep 1', 'Sept 30']  
        if var=='fud':
          if mon_init<=10: cmapcb=[9,0,1,2,3,5,7,11,13,15,16,17,19,10]
          if mon_init==11: cmapcb=[9,9,9,2,3,5,7,11,13,15,16,17,19,10]
          if mon_init==12: cmapcb=[9,9,9,9,9,5,7,11,13,15,16,17,19,10]
          clevsi = np.append(jdm1[18:29+1],jdm1[30]-1)
          cbticks= np.append(jdm[9:14+1],jdm[15]-1) 
          cbticklabels=['Oct 1', 'Nov 1', 'Dec 1','Jan 1','Feb 1', 'Mar 1', 'Mar 30']  
        ##plot params
        pcparams=dict(cmap=cmapj,cmapi=cmapcb,clevs=clevsi)  
        bmparams=dict(region='nps3',coastlinewidth=0.1,
                    oceanmask=True,oceanfillcol=[0.882,0.882,0.882],
                    landmask=True,landfillcol=[0.6,0.6,0.6])  
        ############################################################################      
        #######RAW##################################################################
        ############################################################################      
        #######Plot 1 mod###########
#        fig, axs = plt.subplots(1,1, figsize=(8,8))
        fig=plt.figure(figsize=(8,8))        
        plt.subplots_adjust(wspace=0.05,hspace=0.15,right=0.4,bottom=0.45)
        bm=rpl.make_bm(111,**bmparams) 
        pc=rpl.add_pc(bm,lon,lat,fc12_em,**pcparams)  
        rpl.add_title(bm,'{} raw (i{}m{})'.format(var,fc_year_inits,mon_inits))
        ## colorbar
        rpl.add_cb(bm,pc,manticks=cbticks,manlabels=cbticklabels,lscale=0.95)
        ## save
        rpl.mysavefig(fig,plotdir+'{}_tn{}_fc_i{}m{}_raw.png'.format(var,td,fc_year_inits,mon_inits))
	#######Plot 2#############
        fig=plt.figure(figsize=(8,8))        
        plt.subplots_adjust(wspace=0.05,hspace=0.15,right=0.83,bottom=0.47)
        fig.suptitle('{} raw initialization: {} m{}'.format(var,fc_year_inits,mon_inits),y=0.9)	
        ## plot a
        bm=rpl.make_bm(131,**bmparams) 
        pc=rpl.add_pc(bm,lon,lat,fc1_em,**pcparams)  
        rpl.add_title(bm,mod1)
        bm.scatter(lon[205],lat[-19],transform=ccrs.PlateCarree(),edgecolors='None',color='orange',s=2)
        ## plot b
        bm=rpl.make_bm(132,**bmparams) 
        pc=rpl.add_pc(bm,lon,lat,fc2_em,**pcparams)  
        rpl.add_title(bm,mod2)
        bm.scatter(lon[205],lat[-19],transform=ccrs.PlateCarree(),edgecolors='None',color='orange',s=2)
        ## plot c
        bm=rpl.make_bm(133,**bmparams) 
        pc=rpl.add_pc(bm,lon,lat,fc12_em,**pcparams)  
        rpl.add_title(bm,'CanSIPSv2')
        ## colorbar
        rpl.add_cb(bm,pc,manticks=cbticks,manlabels=cbticklabels,orientation='horizontal',lscale=3,x0scale=-1.13,y0scale=-1.1)
        ## save
        rpl.mysavefig(fig,plotdir+'2mods/{}_tn{}_fc_i{}m{}_raw_2mods.png'.format(var,td,fc_year_inits,mon_inits))
        ############################################################################          
        #######BIAS-CORRECTED#########################################################
        ############################################################################      
        #######Plot 1 mod###########
        fig=plt.figure(figsize=(8,8))
        plt.subplots_adjust(wspace=0.05,hspace=0.15,right=0.4,bottom=0.45)
        bm=rpl.make_bm(111,**bmparams) 
        pc=rpl.add_pc(bm,lon,lat,fc12_em_bc,**pcparams)  
        rpl.add_title(bm,'{} bc (i{}m{})'.format(var,fc_year_inits,mon_inits))
        ## colorbar
        rpl.add_cb(bm,pc,manticks=cbticks,manlabels=cbticklabels,lscale=0.95)
        ## save
        rpl.mysavefig(fig,plotdir+'{}_tn{}_fc_i{}m{}_bc_cf_{}.png'.format(var,td,fc_year_inits,mon_inits,base_period))
        #######Comparison with obs###########
        if verify:
          #######Plot bc###########
          fig=plt.figure(figsize=(8,8))
          sbpt=[121,122]
          fig.subplots_adjust(wspace=0.35,hspace=0.15,right=0.83,bottom=0.45)
          fig.suptitle('{} initialization: {} m{}'.format(var,fc_year_inits,mon_inits),y=0.95)
          ## plot a
          bm=rpl.make_bm(sbpt[0],**bmparams) 
          pc=rpl.add_pc(bm,lon,lat,fc12_em_bc,**pcparams)  
          rpl.add_title(bm,'CanSIPS Model')
          rpl.add_cb(bm,pc,manticks=cbticks,x0scale=1.0,manlabels=cbticklabels,lscale=0.95)
          ## plot b
          bm=rpl.make_bm(sbpt[1],**bmparams) 
          pc=rpl.add_pc(bm,lon,lat,obs,**pcparams)  
          rpl.add_title(bm,'Observed')
          rpl.add_cb(bm,pc,manticks=cbticks,x0scale=1.0,manlabels=cbticklabels,lscale=0.95)
          ## colorbar
          ## save
          rpl.mysavefig(fig,plotdir+'{}_fc_vs_obs_i{}m{}_tn{}.png'.format(var,fc_year_inits,mon_inits,td))            
        #######Plot 2 mods###########
        fig=plt.figure(figsize=(8,8))
        plt.subplots_adjust(wspace=0.05,hspace=0.15,right=0.83, bottom=.47)
        fig.suptitle('{} bias-corrected initialization: {} m{}'.format(var,fc_year_inits,mon_inits),y=0.9)
        ## plot a
        bm=rpl.make_bm(131,**bmparams) 
        pc=rpl.add_pc(bm,lon,lat,fc1_em_bc,**pcparams)  
        rpl.add_title(bm,'GEM-NEMO')        
        #bm.scatter(lon[225],lat[-15],transform=ccrs.PlateCarree(),edgecolors='None',color='green',s=2)  
        ## plot b
        bm=rpl.make_bm(132,**bmparams) 
        pc=rpl.add_pc(bm,lon,lat,fc2_em_bc,**pcparams)  
        rpl.add_title(bm,'CanCM4i')
        #bm.scatter(lon[225],lat[-15],transform=ccrs.PlateCarree(),edgecolors='None',color='green',s=2)
        ## plot c
        bm=rpl.make_bm(133,**bmparams) 
        pc=rpl.add_pc(bm,lon,lat,fc12_em_bc,**pcparams)  
        rpl.add_title(bm,'CanSIPSv2')
        ## colorbar
        rpl.add_cb(bm,pc,manticks=cbticks,manlabels=cbticklabels,orientation='horizontal',lscale=3,x0scale=-1.13,y0scale=-1.1)
        ## save
        rpl.mysavefig(fig,plotdir+'2mods/{}_tn{}_fc_i{}m{}_bc_cf_{}_2mods.png'.format(var,td,fc_year_inits,mon_inits,base_period))
        ############################################################################          
        #######ANOMALY#########################################################
        ############################################################################      
        ## colors, levels
        cmapj='blue0red19'
        if var=='ifd': cmapcb=[18,17,16,14,12,10,8,6,4,2,1,0]
        if var=='fud': cmapcb=[0,1,2,4,6,8,10,12,14,16,17,18]
        clevsi = [-45,-35,-25,-15,-5,0,5,15,25,35,45]
        cbticks= np.arange(-45,55,10) 
        pcparams=dict(cmap=cmapj,cmapi=cmapcb,clevs=clevsi)  
        bmparams=dict(region='nps3',coastlinewidth=0.1,
                 oceanmask=True,oceanfillcol=[0.882,0.882,0.882],
                 landmask=True,landfillcol=[0.6,0.6,0.6])
        #######Plot 1 mod###########
        fig=plt.figure(figsize=(8,8))        
        plt.subplots_adjust(wspace=0.05,hspace=0.15,right=0.4,bottom=0.45)
        ## plot 
        bm=rpl.make_bm(111,**bmparams) 
        pc=rpl.add_pc(bm,lon,lat,fc12_em_anom,**pcparams)
        rpl.add_title(bm,'{} anom (i{}m{}), cf {}'.format(var,fc_year_inits,mon_inits,base_period))
        ## colorbar
        rpl.add_cb(bm,pc,manticks=cbticks,units='days',lscale=0.95)
        ## save
        rpl.mysavefig(fig,plotdir+'{}_tn{}_fc_i{}m{}_anom_cf_{}.png'.format(var,td,fc_year_inits,mon_inits,base_period))
        #######Comparison with obs###########
        if verify:
          #######Plot anomaly###########
          fig=plt.figure(figsize=(8,8))
          plt.subplots_adjust(wspace=0.25,hspace=0.15,right=0.83,bottom=0.45)
          fig.suptitle('{} anomaly initialization: {} m{} (cf {})'.format(var,fc_year_inits,mon_inits,base_period),y=0.95)
          ## plot a
          bm=rpl.make_bm(121,**bmparams) 
          pc=rpl.add_pc(bm,lon,lat,fc12_em_anom,**pcparams)  
          rpl.add_title(bm,'CanSIPS Model')
          rpl.add_cb(bm,pc,manticks=cbticks,x0scale=1.0)
          ## plot b
          bm=rpl.make_bm(122,**bmparams) 
          pc=rpl.add_pc(bm,lon,lat,obs_anom,**pcparams)  
          rpl.add_title(bm,'Observed')
          rpl.add_cb(bm,pc,manticks=cbticks,x0scale=1.0,units='days')
          ## colorbar
          ## save
          rpl.mysavefig(fig,plotdir+'{}_fc_vs_obs_i{}m{}_anom_tn{}_cf_{}.png'.format(var,fc_year_inits,mon_inits,td,base_period))            
        #######Plot 2 mods###########
        fig=plt.figure(figsize=(8,8))
        plt.subplots_adjust(wspace=0.05,hspace=0.15,right=0.83,bottom=0.47)
        fig.suptitle('{} anomaly initialization: {} m{} (cf {})'.format(var,fc_year_inits,mon_inits,base_period),y=0.9)
        ## plot a
        bm=rpl.make_bm(131,**bmparams) 
        pc=rpl.add_pc(bm,lon,lat,fc1_em_anom,**pcparams)  
        rpl.add_title(bm,'GEM-NEMO')
        ## plot b
        bm=rpl.make_bm(132,**bmparams) 
        pc=rpl.add_pc(bm,lon,lat,fc2_em_anom,**pcparams)  
        rpl.add_title(bm,'CanCM4i')
        ## plot c
        bm=rpl.make_bm(133,**bmparams) 
        pc=rpl.add_pc(bm,lon,lat,fc12_em_anom,**pcparams)  
        rpl.add_title(bm,'CanSIPSv2')
        ## colorbar
        rpl.add_cb(bm,pc,manticks=cbticks,orientation='horizontal',
                 lscale=3,x0scale=-1.13,y0scale=-1.1,units='days')
        ## save
        rpl.mysavefig(fig,plotdir+'2mods/{}_tn{}_fc_i{}m{}_anom_cf_{}_2mods.png'.format(var,td,fc_year_inits,mon_inits,base_period))
        ###############################################################################
        #######WRITE BIAS CORRECTED VALUES ############################################
        ###############################################################################
        ## Make netcdf files of annual fud and ifd    
        ## set up output file
        outfile = datadir+'{}_tn{}_bc_cf_{}_i{}m{}_CanSIPSv2.nc'.format(var,td,base_period,fc_year_inits,mon_inits)
        outnc = Dataset(outfile,'w',format='NETCDF3_CLASSIC')
        outtime = outnc.createDimension('time', None) 
        outlon = outnc.createDimension('longitude',np.size(lon))
        outlat = outnc.createDimension('latitude',np.size(lat))  
        outlats = outnc.createVariable('latitude','d',('latitude'))
        outlons = outnc.createVariable('longitude','d',('longitude'))                    
        outfld = outnc.createVariable(var,'d',('latitude','longitude'),fill_value=-999)
        outfld.units = 'Julian Day'
        outfld.long_name = var  
        outlats.units = 'degrees_north'
        outlats.long_name = 'Latitude'
        outlats.standard_name = 'latitude'
        outlons.units = 'degrees_east'
        outlons.long_name = 'Longitude'
        outlons.standard_name = 'longitude'        
        outnc.creation_date = time.ctime(time.time())
        outnc.created_by = user_string+', git hash: '+hash
        outlats[:] = lat
        outlons[:] = lon
        outfld[:] = fc12_em_bc
        outnc.close()
        ###############################################################################
        #######WRITE ANOMALIES########### ############################################
        ###############################################################################
        ## Make netcdf files of annual fud and ifd anomalies   
        ## set up output file
        outfile = datadir+'{}_tn{}_anom_cf_{}_i{}m{}_CanSIPSv2.nc'.format(var,td,base_period,fc_year_inits,mon_inits)
        outnc = Dataset(outfile,'w',format='NETCDF3_CLASSIC')
        outtime = outnc.createDimension('time', None) 
        outlon = outnc.createDimension('longitude',np.size(lon))
        outlat = outnc.createDimension('latitude',np.size(lat))  
        outlats = outnc.createVariable('latitude','d',('latitude'))
        outlons = outnc.createVariable('longitude','d',('longitude'))                    
        outfld = outnc.createVariable(var,'d',('latitude','longitude'),fill_value=-999)
        outfld.units = 'Julian Day'
        outfld.long_name = var  
        outlats.units = 'degrees_north'
        outlats.long_name = 'Latitude'
        outlats.standard_name = 'latitude'
        outlons.units = 'degrees_east'
        outlons.long_name = 'Longitude'
        outlons.standard_name = 'longitude'        
        outnc.creation_date = time.ctime(time.time())
        outnc.created_by = user_string+', git hash: '+hash
        outlats[:] = lat
        outlons[:] = lon
        outfld[:] = fc12_em_anom
        outnc.close()  
        #######TESTPLOT#########################################################
        if testplot:
          fig, axs = plt.subplots(1,1, figsize=(8,8))
          plt.subplots_adjust(wspace=0.05,hspace=0.15,right=0.83,bottom=0.45)
          axs.plot(hc_years,np.mean(hc1_ts[:,:,tlati,tloni],axis=1),label=mod1,color='blue')
          axs.plot(hc_years,np.mean(hc2_ts[:,:,tlati,tloni],axis=1),label=mod2,color='orange')
          axs.plot(hc_years,obs_ts[:,-5,34],label='OBS',color='green')
          axs.scatter(2020,fc1_em[-5,34],color='blue')
          axs.scatter(2020,fc2_em[-5,34],color='orange')
          axs.scatter(2020,fc1_em_bc[-5,34],color='blue',marker='*')
          axs.scatter(2020,fc2_em_bc[-5,34],color='orange',marker='*')
          rpl.add_title(axs,'IFD, i2020m05, lon='+str(lon[tloni])+'E lat='+str(lat[tlati])+'N')
          axs.legend()
          rpl.mysavefig(fig,plotdir+'{}_tn{}_timeseries_i{}m{}_anom_cf_{}_testpoint.png'.format(var,td,fc_year_inits,mon_inits,base_period))




