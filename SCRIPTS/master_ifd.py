#!/usr/bin/python
import os
import glob 
import re 
import subprocess as sbp
import time 
import sys
from optparse import OptionParser
# Set the flags for desired actions
do_download_obs=False # If True, update 0a_download_nrt_sic.sh & 0b_download_v03_sic.sh
do_update_obs_sic_timeseries=False # If True, update 1_interp_sic_obs.py
do_calculate_raw_ifd=False # If True, update 2_calc_ifd+fud.py
do_calc_plot_skill=True # If True, update 3a_calc+plot_skill.py 
do_calc_plot_forecast=True # If True, update 3b_calc+plot_fc.py
#
do_change_advanced_parms=False # If True, use ifd_adv_parms as well. Dictionary entries must be added.
do_run_scripts=True # If True, run and delete temporary scripts. If False, just make the required scripts
##########################################################################################################
# Make dictionary of basic parameters to set: Set parameters leaving quotes as they are.
##########################################################################################################
ifd_parms={}
# parameters used in multiple scripts:
#ifd_parms['user_string']='''"Michael Sigmond, CCCma"''' # Information about user to include in netcdf files.
#ifd_parms['basedir']='''"/space/hall3/sitestore/eccc/crd/ccrn/users/rms101/SCRIPTS/ifd_fud/"''' # root of DATA/PLOTS directory
#ifd_parms['codedir']='''"/space/hall3/sitestore/eccc/crd/ccrn/users/rms101/SCRIPTS/ifd_fud/"''' # root of directory containing code templates and local modules
#ifd_parms['rundir']='''"/space/hall3/sitestore/eccc/crd/ccrn/users/rms101/SCRIPTS/ifd_fud/TMP/"''' # directory to make temporary files in
ifd_parms['user_string']='''"Cathy Reader, CCCma"''' # Information about user to include in netcdf files.
ifd_parms['basedir']='''"/space/hall3/sitestore/eccc/crd/ccrn/users/mcr001/ifd_fud/"''' # root of DATA/PLOTS directory
ifd_parms['codedir']='''"/home/mcr001/IFD_op/ifd_fud/"''' # root of directory containing code templates and local modules
ifd_parms['rundir']='''"/space/hall3/sitestore/eccc/crd/ccrn/users/mcr001/tmp/"''' # directory to make temporary files in
ifd_parms['firstyearfo']=1981 # first year of entire obs record
ifd_parms['firstmonthfo']=1 # first month of entire obs record
ifd_parms['lastyearfo']=2022 # last year of entire final obs record
ifd_parms['lastmonthfo']=4 # last month of entire final obs record
ifd_parms['nrt_year']=2022 # year that near-real-time data starts in final processed sic obs file
ifd_parms['nrt_month']=1 # momth that near-real-time data starts in final processed sic obs file
#           note that all the nrt data to add must be in the same year. To add
#           nrt data spanning 2 or more years, this script must be run multiple times,
#           once for each year
ifd_parms['nc_version']='''"v04r00"''' # version of processed data
ifd_parms['nrt_version']='''"v02r00"''' # version of nrt data
ifd_parms['mon_init_list']='np.array([5])' # list of initialization months to process
ifd_parms['threshold']=.5 # sea-ice concentraton fracton necessary to qualfy as frozen/melted
ifd_parms['nsd']=10 # number of contnuous days above/below threshold necessary to qualfy as frozen/melted
#parameters used only in download scripts:
ifd_parms['ncdir']='''"G02202_V4"''' # ftp subdirectory to target for processed data 
ifd_parms['nrtdir']='''"G10016_V2"'''# ftp subdirectory to target for processed nrt data
#parameters used in 1_interp_sic_obs.py only:
do_append_to_existing_sic_timeseries=True # if true, new data will be appended to existing sic timeseries
                                         # and parameters below must be set
if do_append_to_existing_sic_timeseries:
  ifd_parms['firstyeara']=2022 # first year of new data to append
  ifd_parms['firstmontha']=1 # first month of new data to append
  ifd_parms['lastyearin']=2021 # last year of file to append to
  ifd_parms['lastmonthin']=12 # last month of file to append to
  ifd_parms['nrt_yearf']=2022 # year that near-real-time data starts in file to append to      
  ifd_parms['nrt_monthf']=1 # month that near-real-time data starts in file to append to      
else:  # these parameters are set automatically in this case
  ifd_parms['firstyeara']=ifd_parms['firstyearfo'] # 
  ifd_parms['firstmontha']=ifd_parms['firstmonthfo'] # 
  ifd_parms['lastyearin']=ifd_parms['firstyeara'] # 
  ifd_parms['lastmonthin']=ifd_parms['firstmontha'] # 
  ifd_parms['nrt_yearf']=ifd_parms['nrt_year'] # 
  ifd_parms['nrt_monthf']=ifd_parms['nrt_month'] # 
#parameters used in 2_calc_ifd+fud.py only#
#ifd_parms['indirpreff']='''"/space/hall3/sitestore/eccc/crd/ccrn/users/rms101/DATA/CanSIPS/crd-data-donnees-rdc.ec.gc.ca/pub/CCCMA/bmerryfield/goapp/goapp/SIPN2/"''' # root directory of forecast sic data
#ifd_parms['indirpreff']='''"/space/hall3/sitestore/eccc/crd/ccrn/users/wsl001/C3S_sic/"''' # root directory of forecast sic data
ifd_parms['indirpreff']='''"/space/hall3/sitestore/eccc/crd/ccrn/users/mcr001/tmp/CanSIPS_May_data/"''' # root directory of forecast sic data 
ifd_parms['firstyearm']=1990 # first year to calculate IFDs/FUDs for models
ifd_parms['firstmonthm']=1 # first month to calculate IFDs/FUDs  " "
ifd_parms['lastyearm']=2022 # last year to calculate IFDs/FUDs " "
ifd_parms['lastmonthm']=5 # last month to calculate IFDs/FUDs  " "
ifd_parms['firstyearo']=1990 # first year to calculate IFDs/FUDs for obs
ifd_parms['firstmontho']=1 # first month to calculate IFDs/FUDs  " "
ifd_parms['lastyearo']=2021 # last year to calculate IFDs/FUDs  " "
ifd_parms['lastmontho']=12 # last month to calculate IFDs/FUDs  " "
#parameters used in 3a_calc+plot_skill.py only:
ifd_parms['firstyearcs']=1990 # first year to calculate hindcast skill
ifd_parms['lastyearcs']=2020 # last year to calculate hindcast skill
#parameters used in 3b_calc+plot_fc.py only:
ifd_parms['fc_year_init']=2021 # initialization year of forecast                                                                                                  
ifd_parms['base_period_list']='''['2012_2020']''' # the period relative to which anomalies are calculated and the period used for bias correction
ifd_parms['hc_period']='''"1990_2020"''' # the years to consider for hindcast. often <firstyearfo>_<lastyearfo>
ifd_parms['varlist']='''['ifd']''' # variables to calculate and plot forecast for 
ifd_parms['verify']=False # if True then observations are available for verification   

########################################
# Dictionary of "advanced" parameters to change. If do_change_advanced_parms, desired dictionary
# entries must be added to ifd_adv_parms, below.
ifd_adv_parms={}
run_1_twice=False # Will be switched to True if nrt data needs to be added separately
# Set ifd_parms in this script      
for i in ifd_parms.keys():
   exec('{KEY} ={VALUE}'.format(KEY=i,VALUE=repr(ifd_parms[i])))
if not os.path.isdir(rundir[1:-1]):os.makedirs(rundir[1:-1])
# Set firstyearo (first year to calculate IFDs/FUDs from obs) etc. to firstyearfo etc.
# To do a subset, comment out this section and add dictionary entries for these. This
# will not generally be necessary since the other scripts can work off full-length files.
#for fl in ['first','last']:
#   for ym in ['year','month']: 
#      ifd_parms[fl+ym+'o']=eval(fl+ym+'m')
#if ifd_parms['lastmonthfo']!=12:      
#   ifd_parms['lastmontho']=12
#   ifd_parms['lastyearo']=lastyearfo-1  
# make "time" strings out of years and months
nrt_time=str(nrt_year)+"%02d"%(nrt_month)
lasttimefo=str(lastyearfo)+"%02d"%(lastmonthfo)
firsttimea=str(firstyeara)+"%02d"%(firstmontha)
# Figure out which scripts to edit and run
infile_list=[]
if do_download_obs: infile_list.append('0a_download_nrt_sic.sh'); infile_list.append('0b_download_processed_sic.sh')
if do_update_obs_sic_timeseries: infile_list.append('1_interp_sic_obs.py')
if do_update_obs_sic_timeseries and nrt_time <= lasttimefo and nrt_time > firsttimea:
   infile_list.append('1_interp_sic_obs.py') # add another run of 1_interp_sic_obs.py for nrt data
   run_1_twice=True
if do_calculate_raw_ifd: infile_list.append('2_calc_ifd+fud.py')
if do_calc_plot_skill: infile_list.append('3a_calc+plot_skill.py')
if do_calc_plot_forecast: infile_list.append('3b_calc+plot_fc.py')
ran_1_once=False
# loop through ifd_parms and make temporory scripts with desired parameters.
for i in infile_list:
 lines=[]
 if i=='1_interp_sic_obs.py': # parameters depending on run ot 1_interp_sic_obs.py
    if ran_1_once==True:
       print('set up dictionary for second run of '+i)
       ifd_parms['firstyeara']=nrt_year
       ifd_parms['firstmontha']=nrt_month       
       ifd_parms['lastyeara']=lastyearfo
       ifd_parms['lastmontha']=lastmonthfo
       lastmonthin=nrt_month-1                                                   
       lastyearin=nrt_year
       if lastmonthin ==0: lastyearin=nrt_year-1; lastmonthin=12   
       ifd_parms['lastyearin']=lastyearin
       ifd_parms['lastmonthin']=lastmonthin       
       ifd_parms['lastmontha']=lastmonthfo
       ifd_parms['nrt_yearf']=nrt_year
       ifd_parms['nrt_monthf']=nrt_month       
    else:
       if run_1_twice:
          lastmontha=nrt_month-1
          lastyeara=nrt_year
          if lastmontha ==0: lastyeara=nrt_year-1; lastmontha=12             
          ifd_parms['lastyeara']=min(lastyeara,lastyearfo)          
          print('set up dictionary for first of two runs of '+i)
          ifd_parms['lastmontha']=lastmontha
       else:
          print('set up dictionary for running '+i+' once only')
          ifd_parms['lastyeara']=lastyearfo
          ifd_parms['lastmontha']=lastmonthfo
       ran_1_once=True
# print(ifd_parms)
# write version of script to run       
 with open(codedir[1:-1]+'SCRIPTS/'+i, "r") as infile:
    print('setting parameters in '+ i)   
    for line in infile:
        if do_change_advanced_parms:
            for var in ifd_adv_parms.keys():
                oldstr='^'+var+'=.*'
                newstr=var+'='+str(ifd_parms[var])
                line=re.sub(oldstr,newstr,line)
        for var in ifd_parms.keys():
            oldstr='^'+var+'=.*'
            newstr=var+'='+str(ifd_parms[var])
            line=re.sub(oldstr,newstr,line)
        lines.append(line)
 outfilepath = rundir[1:-1]+i       
 outfile = open(outfilepath,"w")
 for j in lines:
   outfile.write(j)  
 outfile.close()
 if i=='1_interp_sic_obs.py' and run_1_twice and not do_run_scripts: 
   mvc='mv '+outfilepath + ' ' +outfilepath+'_firstyeara_'+str(ifd_parms['firstyeara'])+'_script'
   print(mvc)
   sbp.call(mvc,shell=True)
 if do_run_scripts:
   filename, filext = os.path.splitext(outfilepath)
   if filext == '.py':
     rsc='python '+ outfilepath
   elif filext =='.sh':
     rsc='bash '+ outfilepath
   print('Running '+i)  
   sbp.call(rsc,shell=True)
   rmt='/bin/rm -r '+outfilepath
   sbp.call(rmt,shell=True)
   






                                       
